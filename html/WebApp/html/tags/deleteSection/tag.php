<?php
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, title, wrapper_id	***/
	function ct_deleteSection($tag)
	{
		return '
		<div>
			<ct:button 	id="deleteButton" 	text="Delete Selected" 	onclick="deleteSequence()"	caution="true"/>
			<ct:button 	id="clearButton" 	text="Delete All" 		onclick="clearList()"		caution="true"/>
		</div>';
	}
?>