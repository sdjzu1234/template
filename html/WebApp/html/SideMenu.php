<?php

    namespace CustomTags;

	ini_set('error_reporting', E_ALL);
	ini_set('track_errors', '1');
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	
	$current_dir = dirname(__FILE__).DIRECTORY_SEPARATOR;

	require_once dirname($current_dir).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'CustomTags.php';
	
	$ct = new CustomTags(array(
		'parse_on_shutdown' 	=> true,
		'tag_directory' 		=> $current_dir.'tags'.DIRECTORY_SEPARATOR,
		'sniff_for_buried_tags' => true
	));
?>


<html>
	<body>
		<script type="text/javascript" src="../javascript/functions.js"></script>
		
		<ct:loading 	id="overlay" 		img_src="../images/loading5.gif"/>          
    <div id="menuTitle" class="menutitle">
            SimSpliceEvol
    </div><br>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-1.3.2.min.js"></script>


<style>  

fieldset 
  {
		border: 1px solid #253b24 !important;
		margin: 0;
		xmin-width: 0;
		padding: 1px;       
		position: relative;
		border-radius:4px;
		background-color:#93C178;
		box-shadow: 10px 5px 5px black;
	}	
	
legend
		{
			font-size:14px;
			font-weight:bold;
			margin-bottom: 0px; 
			width: 40%; 
			border: 1px solid #ddd;
			border-radius: 4px; 
			padding: 5px 5px 5px 10px; 
			background-color: #ffffff;
		}
#numberOfSimulation{
    padding-left: 10px !important;
    padding-right: 10px !important;
    height: 40px;

}
label{
    padding-left: 0px !important;
    padding-right: 8px !important;
  }
#textarea{
            font-size:12px;
}
#launchSimulation{
            text-align:center;

}
.inputSize{
  width : 60px; 
}
    
</style>
<form id="data" class="form-horizontal form-group-lg was-validated" method="post" enctype="multipart/form-data">

<fieldset>
<legend>Input Guide Tree</legend>

<div class="form-group" id="textareaMode">
  <div class="col-sm-12">                     
    <textarea class="form-control" id="treenw" name="treenw" rows="3">(ggal:0.39452,(mdom:0.216455,((mmus:0.214942,hsap:0.1962098)1:0.1001,btaus:0.21365)1:0.1143738)1:0.1100642);</textarea>
  </div>
  <div class="col-sm-12"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label class="radio-inline" for="radios-1" onclick="switchmode('modePaste')">
      <input type="radio" name="inputmode" id="modePaste" onclick="switchmode('modePaste')" checked value="modePaste">
      Paste a tree
    </label>
    <label class="radio-inline" for="radios-00" onclick="switchmode('modeUpload')">
      <input type="radio" name="inputmode" id="modeUpload" onclick="switchmode('modeUpload')" value="modeUpload" >
      Upload a newick file
    </label>     
    <label class="radio-inline" for="fileUpload">
      <input id="fileUpload" name="fileUpload" class="input-file" type="file" disabled>
    </label>    
  </div>
</div>
</fieldset>

<fieldset>
<legend>Exon-intron parameters</legend>
<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label" for="k_indel">multiplicative constant for codon indel rate</label>
      <div class="col-xs-4">  
        <input type="number" step="0.01" id="k_indel" name="k_indel" min=0 max=1 value="0.5" size="2" class="inputSize" class="form-control is-valid"  required >
      <div class="valid-feedback">
        Looks good!
      </div>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="k_eic">multiplicative constant for exon-intron change</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="k_eic" name="k_eic"  min=0 value="0.5" size="2" class="inputSize" required>
      </div>
    </div>
</div>

<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="k_intron">multiplicative constant for substitution rate in intron</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="k_intron" name="k_intron" min=0 value="1.5" size="2" class="inputSize" required>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="eic_l">relative frequence of exon loss</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="eic_l" name="eic_l" min=0 size="2" value="0.4" class="inputSize" required>
      </div>
    </div>
</div>

<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="k_nb_exons">multiplicative constant for number of exons in gene</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="k_nb_exons"  min=0 name="k_nb_exons" value="0.1" size="2" class="inputSize" required>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="eic_g">relative frequence of exon gain</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="eic_g" name="eic_g" value="0.5" min=0  size="2" class="inputSize" required>
      </div>
    </div>
</div>
<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="eic_d">relative frequence of exon duplication</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="eic_d" name="eic_d" value="0.5" min=0 size="2" class="inputSize" required>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="k_tc">multiplicative constant for transcript change</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="k_tc" name="k_tc" value="5" min=0  size="2" class="inputSize" required>
      </div>
    </div>
</div>

<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_tl">relative frequence of transcript loss</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="tc_tl" name="tc_tl" value="0.4"  min=0 size="2" class="inputSize" required>
      </div>
    </div>
</div>
</fieldset>

<fieldset>
<legend>Alternative splicing parameters</legend>
<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_a5">relative frequence of alternative five prime</label>
      <div class="col-xs-4">  
        <input type="number" step="0.01" id="tc_a5" name="tc_a5" min=0 size="2" class="inputSize" value = 0.1 required>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_a3">relative frequence of alternative three prime</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="tc_a3" name="tc_a3" value="0.1" min=0 size="2" class="inputSize" required>
      </div>
    </div>
</div>

<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_ek">relative frequence of exon skipping</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="tc_ek" name="tc_ek" value="0.1" min=0 size="2" class="inputSize" required>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_me">relative frequence of mutually exclusive</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="tc_me" name="tc_me" value="0.1" min=0 size="2" class="inputSize" required>
      </div>
    </div>
</div>

<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_ir">relative frequence of intron retention</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="tc_ir" name="tc_ir" value="0.05" min=0 size="2" class="inputSize" required>
      </div>
    </div>
    
    <div class="col-xs-6">
      <label class="col-xs-8 control-label"  for="tc_rs">relative frequence of random selection</label>
      <div class="col-xs-4">  
        <input type="number" step=0.01 id="tc_rs" name="tc_rs" value="0.5" min=0 size="2" class="inputSize" required>
      </div>
    </div>
</div>

</fieldset>

<fieldset>
<legend>Execution and results</legend>
<div class="form-group">  

    <div class="col-xs-6">
      <label class="col-xs-6 control-label"  for="validationTooltip03">Number of simulation</label>
      <div class="col-xs-5">  
     
            <select name="numberOfSimulation" id="numberOfSimulation" class="custom-select-lg">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                        <option value="32">32</option>
                        <option value="33">33</option>
                        <option value="34">34</option>
                        <option value="35">35</option>
                        <option value="36">36</option>
                        <option value="37">37</option>
                        <option value="38">38</option>
                        <option value="39">39</option>
                        <option value="40">40</option>
                        <option value="41">41</option>
                        <option value="42">42</option>
                        <option value="43">43</option>
                        <option value="44">44</option>
                        <option value="45">45</option>
                        <option value="46">46</option>
                        <option value="47">47</option>
                        <option value="48">48</option>
                        <option value="49">49</option>
                        <option value="50">50</option>
            </select>            
      </div>
    </div>
    
    <div class="col-xs-4">
      <label class="col-xs-7 control-label"  for="simulationName">Siumlation name</label>
      <div class="col-xs-5">  
        <input type="text" id="simulationName" name="simulationName" size="10" required>
      </div>
    </div>
</div>

<div class="form-group">  

    <div class="col-xs-4">
      <label class="col-xs-7 control-label"  for="email">Your email</label>
      <div class="col-xs-5">  
        <input type="email" id="email" name="email" size="30" required>
      </div>
    </div>
</div>
</fieldset>
		<div id="launchSimulation">
			<a href="#" id="drawButton" class="button button-rounded button-action" onclick="startAnalysis()">Launch Simulation</a><br><br>
		</div>
</form>
	</body>

</html>
