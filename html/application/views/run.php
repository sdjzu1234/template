<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="iso-8859-1" />
    <title>SpliceViz on gralline.dinf.fsci.usherbrooke.ca - Splice variants for a family of genes</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/spliceviz.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fabric.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/biojs-io-fasta.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jspdf.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  </head>
  <body>
	<div id="bd-wrapper" ng-controller="CanvasControls">
		<br />
		<div id="canvasarea"><canvas id="c" width="1200" height="600"></canvas></div>
		<div id="menucontextuel">
			<ul></ul>
		</div>
		<div id="iconeupload">
			<form id="changeInput" method="post" action="<?php echo base_url(); ?>run">
				<textarea name="files_gfasta" style="display:none;"><?php if(isset($input)){ echo $input["files_gfasta"]; } ?></textarea>
				<textarea name="files_tfasta" style="display:none;"><?php if(isset($input)){ echo $input["files_tfasta"]; } ?></textarea>
				<textarea name="files_t2gtxt" style="display:none;"><?php if(isset($input)){ echo $input["files_t2gtxt"]; } ?></textarea>
				<input type="hidden" name="changeInput" value="1" />
				<a href="#" onclick="document.getElementById('changeInput').submit();">
					<img src="<?php echo base_url(); ?>images/upload.png" alt="Recompute" width="40" height="40" />
				</a>
			</form>
		</div>
		<div id="iconereset">
			<img src="<?php echo base_url(); ?>images/reset.png" alt="Reset" width="40" height="40" />
		</div>
		<div id="iconeoptions">
			<img src="<?php echo base_url(); ?>images/settings.png" alt="Menu Options" width="40" height="40" />
		</div>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/spliceviz.js"></script>
		<script id="main">(function() {
			var canvas = this.__canvas = new fabric.Canvas('c');
			fabric.Object.prototype.transparentCorners = false;
			canvas.selection = false;
			var gfasta = <?php echo json_encode($input["files_gfasta"]); ?>;
			var tfasta = <?php echo json_encode($input["files_tfasta"]); ?>;
			var t2gtxt = <?php echo json_encode($input["files_t2gtxt"]); ?>;
			spliceviz(canvas, gfasta, tfasta, t2gtxt);
		})();
		</script>
	</div>
  </body>
</html>