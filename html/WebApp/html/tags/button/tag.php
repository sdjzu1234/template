<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, text, onclick 		***/
	/*** 		Optional		***/
	/*** caution 				***/
	function ct_button($tag)
	{
		$caution = '';
		
		if(property_exists($tag['attributes'], 'caution'))
			$caution = ' button-caution';
		
		return '<a href="#" id='.$tag['attributes']->id.' class="button button-rounded button-action'.$caution.'" onclick="'.$tag['attributes']->onclick.'">'.$tag['attributes']->text.'</a>';
	}
?>