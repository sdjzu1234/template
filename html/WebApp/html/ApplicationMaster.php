<html>
	<head>
		<!-- javascript -->
		<script type="text/javascript" src="../javascript/jquery/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../javascript/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../javascript/ajax.js"></script>
		<script type="text/javascript" src="../javascript/dragable.js"></script>
		<script src="http://d3js.org/d3.v3.min.js"></script>


		<script type="text/javascript" src="../javascript/sidemenu.js"></script>
		<script type="text/javascript" src="../javascript/input.js"></script>
		<script type="text/javascript" src="../javascript/master.js"></script>
		<script type="text/javascript" src="../javascript/buttons.js"></script>
		<script type="text/javascript" src="../javascript/newDendrogram.js"></script>

		<!-- css -->
		<link rel="stylesheet" type="text/css" href="../css/reset.css">
		<link rel="stylesheet" type="text/css" href="../css/master.css">
		<link rel="stylesheet" type="text/css" href="../css/dragable.css">

		<link rel="stylesheet" type="text/css" href="../css/menu.css">
		<link rel="stylesheet" type="text/css" href="../css/input.css">
		<link rel="stylesheet" type="text/css" href="../css/buttons.css">
		<link rel="stylesheet" type="text/css" href="../css/dendrogram.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>	
            
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
	</head>
<body>
<div id="master">
    <div class="row">
      <div class="col-5">
			<div id="menu" class="sidenav">
				<div id="menu-content-wrapper">	
				</div>	

			</div>


			<div id="inputPanel" class="dragable">
				<div id="inputPanelHeader" class="dragableHeader">
					<div id="inputPanelHide" onclick="inputHide()"></div>
				</div>
			    <div id="input-content-wrapper">				
			    </div>
			</div>
      </div>
      <div class="col-7">
      		<div class="row" id="result"> 
	      		<div class="row">
					<div class="">
					  <div class="page-header">
					    <h1>SimSpliceEvol Result for <span id="simulationNameVal"></span> analysis</h1>      
					  </div>
					  	<em>Please cite:</em><br>
						<em><a href="#">"SimSpliceEvol: Alternative splicing-aware simulation of biological sequence evolution"</a></em><br> 
						<em><b>Esaie Kuitche, Safa Jammali and Aı̈da Ouangraoua.</b></em><br> 
						<em>BMC Bioinformatics 2019.</em><br> 
					</div>
	  			</div>
	      		<div class="row">
	                    <div class="container">
						        <table id="table" width="180px" data-height="300" data-toggle="#table" data-unique-id="Keyword" data-sort-name="iteration" data-sort-order="desc" class="table-responsive-sm">
						        <thead>
						            <tr>
						                <th data-field="iteration" data-sortable="true" class="col-sm-1">Iteration</th>
						                <th data-field="number_gene" data-sortable="true" class="col-sm-1">#Gene</th>
						                <th data-field="number_cds" data-sortable="true" class="col-sm-1">#CDS</th>
										<th data-field="mean_nb_cds_per_gene" data-sortable="true"class="col-sm-1">#CDS/gene</th>                                                
						                <th data-field="avg_gene_length" data-sortable="true" class="col-sm-1">#gene len</th>
						                <th data-field="avg_cds_length" data-sortable="true" class="col-sm-1">#cds len</th>
						                <th data-field="PID" data-sortable="true" class="col-sm-1">PID</th>
						                <th data-field="number_cluster" data-sortable="true" class="col-sm-1">#Cluster</th>
						                <!--<th data-field="number_cds_per_cluster" data-sortable="true" class="col-sm-1">#CDS/cluster</th>-->                
						            </tr>
						        </thead>

						    </table>


	                    </div>
	            </div>
	           <div class="row">
	           	<div class="container">
	           	<button class="btnDownload" id="btnDownload" ><i class="fa fa-download"></i> Download (tar format) </button>
	           </div>
	           </div>
            </div>
      </div>
    </div>
</div>	
		
		<div id="footerSlideContainer">
			<div id="dendroPanelTitle"></div>
			<div id="subTitle"></div>
			<div id="dendroListLeft"></div>
			<div id="subTitle2"></div>
			<div id="dendroListRight"></div>
			<div id="footerSlideButton">
				<div id="sliderButtonText">Results</div>
			</div>
			<div id="footerSlideContent">
			</div>
		</div>
		
		
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $("input[type='button']").click(function(){
            var radioValue = $("input[name='gender']:checked").val();
            if(radioValue){
                alert("Your are a - " + radioValue);
            }
        });
        
    });
</script>
</html>