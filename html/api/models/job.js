'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JobSchema = new Schema({
  session: {
    type: String,
    required: true
  },
  command: {
    type: String,
    required: true
  },
  start_date: {
    type: Date,
    default: Date.now
  },
  end_date: {
    type: Date
  },
  status: {
      type: String,
      enum: ['started', 'completed', 'error']
  },
  data: {
    type: String
  },
  error: {
    type: String
  }
});

module.exports = mongoose.model('Jobs', JobSchema);