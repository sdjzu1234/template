<?php
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, title, wrapper_id	***/
	function ct_selectDeselectSection($tag)
	{
		return '
		<div id="rnaList-actionPanel">
			<ct:button 	id="selectButton" 	text="Select All" 		onclick="selectAll()"/>
			<ct:button 	id="unselectButton" text="Unselect All" 	onclick="clearSelection()"/>
		</div>
		';
	}
?>