<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, placeholder 			***/
	function ct_textarea($tag)
	{
		if (property_exists($tag['attributes'], 'placeholder'))
		{
            return '<textarea id='.$tag['attributes']->id.' rows='.$tag['attributes']->rows.' cols='.$tag['attributes']->cols.' placeholder="'.$tag['attributes']->placeholder.'"></textarea>';
        }
        
        return '<textarea id='.$tag['attributes']->id.' rows='.$tag['attributes']->rows.' cols='.$tag['attributes']->cols.'></textarea>';
	}
?>