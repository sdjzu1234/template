<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, img_src 			***/
	function ct_form($tag)
	{
        $formsection = '<form action="/action_page_binary.asp" id="'.$tag['attributes']->id.'" method ="post" enctype="multipart/form-data">';
        $formsection = $formsection.'</form>';

        if (property_exists($tag['attributes'], 'action'))
		    {
            $result = '<form action='.$tag['attributes']->action.' id="'.$tag['attributes']->id.'" method ="post" enctype="multipart/form-data">';
            
            return $result;
        }

        if (property_exists($tag['attributes'], 'type'))
		    {
            if (strpos($tag['attributes']->type , "support") !== false)
            {
                $result = '<form id="'.$tag['attributes']->id.'">';
                $result = $result.'
                <label for="nameAnalysis">Name of your analysis : </label>
                <ct:text id="nameAnalysis"	name="analysisname" text=""/><br>
                <label for="youremail">your email : </label>
                <ct:text id="email" name="email" text=""/><br>
                <label for="confirmemail">confirm your email : </label>
                <ct:email id="drawEmail" 	name="emailconfirm" text="" placeholder="confirm your email"/><br>
                <ct:textarea id="comment" rows="4" cols="60" text="comment" placeholder="Enter your comment here..."/>
                <br><br>';
                $result = $result.'</form>';
            }
            else if (strpos($tag['attributes']->type , "input") !== false)
            {
                $result = '<form action="/action_page_binary.asp" id="'.$tag['attributes']->id.'" method ="post" enctype="multipart/form-data">';
                $result = $result.'<b>Name of your input:</b> <input type="text" name="analysisname">';
                $result = $result.'</form>';
            }
            
            return $result;
        }

        
        $result = $formsection;
    
        return $result;   
    }
?>