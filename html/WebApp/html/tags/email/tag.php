<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, img_src 			***/
	function ct_email($tag)
	{
		if (property_exists($tag['attributes'], 'placeholder'))
		{
			return $tag['attributes']->text.": ".'<input type= "email" id="'.$tag['attributes']->id.'" name="'.$tag['attributes']->name.'" placeholder="'.$tag['attributes']->placeholder.'"><br>';
		}

		return $tag['attributes']->text.": ".'<input type= "email" id="'.$tag['attributes']->id.'" name="'.$tag['attributes']->name.'"><br>';
	}
?>