<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, text, onclick 		***/
	/*** 		Optional		***/
	/*** caution 				***/
	function ct_radio($tag)
	{
		$options_number_radioBtn = '';
		$options_radioBtn = '';
		$options_radioBtn_text = '';
		$result = '';

		/*more than one btn radio at the time*/
		if (property_exists($tag['attributes'], 'number'))
		{
			$index = 0;
			$options_number_radioBtn = $tag['attributes']->number;
			$options_radioBtn_value = explode("|", $tag['attributes']->value);
			$options_radioBtn_text = explode("|", $tag['attributes']->text);

			foreach ($options_radioBtn_value as $value) {
				$result = $result.'<input type="radio" id='.$tag['attributes']->id.' name='.$tag['attributes']->name.' value='.$value.'> '.$options_radioBtn_text[$index].'<br>';
				$index++;
			}
			return $result;

		}
		
		// one radio button return
		$result = '<input type="radio" id='.$tag['attributes']->id.' name='.$tag['attributes']->name.' value='.$tag['attributes']->value.'> '.$tag['attributes']->text.'<br>';
		
		return $result;
	}
?>