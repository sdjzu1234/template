'use strict';
var cmd = require('node-cmd');
var fs = require('mz/fs');
var mongoose = require('mongoose');
var path = require('path');
var zipdir = require('zip-dir');

var Job = mongoose.model('Jobs');

var Tool = require('../config/tool.json');

const INPUT_PATH = "../app/inputs/";
const RESULT_PATH = "../app/results/";
const RESULT__ARCHIVE_PATH = "../app/results.zip";

/**
 * Execute a new job
 * @param {object} req - Request object
 * @param {object} res - Response object
 */
exports.execute = async function (req, res) {
  var message = "";
  var command = "";
  var session = req.params.session;

  if (!session) {
    res.json({ "message": "Error : Missing session identifier" });
  }

  await createSessionDirectories(session);

  command = Tool.base_command;
  command += await getParameters(Tool.params, req);

  try {
    startExecution(session, command);
    message = "Execution started";
  } catch(error) {
    message = error;
  }

  res.json({
    "message": message,
    "command": command,
    "session_id": session
  });
};

/**
 * Create session directories for inputs and results, if it doesn't already exist
 * @param {string} sessionId - Id of the current session
 */
async function createSessionDirectories(sessionId) {
  try {
    await fs.mkdir(INPUT_PATH + sessionId);
  } catch (error) {
    if (error.code != 'EEXIST') throw error;
  }

  try {
    await fs.mkdir(RESULT_PATH + sessionId);
  } catch (error) {
    if (error.code != 'EEXIST') throw error;
  }
}

/**
 * Parse all parameters from request
 * @param {object} params - Parameters from tool configuration
 * @param {object} req - Request object
 */
async function getParameters(params, req) {
  var session = req.params.session;
  var line = "";

  params = replaceParamsID(params, session);
  params = replaceParamsBasePath(params);

  for (const config of params) {
    var value = req.body[config.name];

    if (config.value_type == "None") {
      line += getStaticParameterValue(value, config);
    } else {
      var res = await getUserDefinedParameterValue(session, value, config);
      line += res;
    }
  }

  return line;
}

/**
 * Replace session ID placeholder by the value in all parameters
 * @param {array} params - List of parameters
 * @param {string} sessionId - Current session ID
 */
function replaceParamsID(params, sessionId) {
  params.forEach(config => {
    config.prefix = config.prefix.replace(/\{\{id\}\}/g, sessionId);
  });
  return params;
}

/**
 * Replace base path placeholder by the value in all parameters
 * @param {array} params - List of parameters
 */
function replaceParamsBasePath(params) {
  params.forEach(config => {
    config.prefix = config.prefix.replace(/\{\{base_path\}\}/g, path.resolve(__dirname, ".."));
  });
  return params;
}

/**
 * Replace session ID placeholder by the value in the result
 * @param {string} result - Result of the execution 
 * @param {string} sessionId - Current session ID
 */
function replaceResultID(result, sessionId) {
  return result.replace(/\{\{id\}\}/g, sessionId);
}

/**
 * Replace base path placeholder by the value in the result
 * @param {string} result - Result of the execution
 */
function replaceResultBasePath(result) {
  return result.replace(/\{\{base_path\}\}/g, path.resolve(__dirname, ".."));
}

/**
 * Return parameter string for a static parameter
 * @param {string} value - Request value
 * @param {object} config - Parameter configuration
 */
function getStaticParameterValue(value, config) {
  if (!config.optional || !value) {
    return " " + config.prefix;
  } else {
    return "";
  }
}

/**
 * Return parameter string for a user-defined parameter
 * @param {string} sessionId - Id of the current session
 * @param {string} value - Request value
 * @param {object} config - Parameter configuration 
 */
async function getUserDefinedParameterValue(sessionId, value, config) {
  try {
    if (value != undefined) {
      if (config.value_type == "File") {
        var pathVal = INPUT_PATH + sessionId + '/' + config.file_name;
        await fs.writeFile(pathVal, value);
        var absolutePath = path.resolve(pathVal);
        return " " + config.prefix + " " + absolutePath;
      } else {
        return " " + config.prefix + " " + value;
      }
    } else if (!config.optional) {
      throw "Error : Missing parameter";
    }
  } catch (err) {
    throw "Unknown error : " + err;
  }
}


async function startExecution(session, command) {
  try {
    var currentJob = await Job.findOne({ session: session });
    if (!currentJob) {
      currentJob = new Job({
        session: session,
        command: command,
        status: 'started'
      });
    } else {
      currentJob.status = 'started';
      currentJob.start_date = Date.now();
    }
    await currentJob.save();

    cmd.get("cd ../app/SpliceFamAlignMulti\n" + command, async (err, data, stderr) => {
      if (err) {
        currentJob = await Job.findOne({ session: session });
        currentJob.status = 'error';
        currentJob.error = err;
        currentJob.end_date = Date.now();
        currentJob.save();
      } else if (stderr) {
        currentJob = await Job.findOne({ session: session });
        currentJob.status = 'error';
        currentJob.error = stderr;
        currentJob.end_date = Date.now();
        currentJob.save();
      } else {
        currentJob = await Job.findOne({ session: session });
        currentJob.status = 'completed';
        currentJob.end_date = Date.now();
        currentJob.save();
      }
    });
  } catch (error) {
    throw "Error while executing command : " + error;
  }
}




/**
 * Read results of a job
 * @param {object} req - Request object
 * @param {object} res - Response object
 */
exports.read = async function (req, res) {
  var job = await Job.findOne({ session: req.params.session });

  if (job.status == 'started') {
    res.json(
      {
        "status": "pending",
        "message": "Execution still in progress"
      }
    );
  } else if (job.status == 'error') {
    res.status(500).json(
      {
        "status": "error",
        "message": "An error occured during the execution",
        "error": job.error
      }
    );
  } else {
    var res_data = {};

    for (const result of Tool.results) {
      res_data[result.name] = { "type": result.type };
      if (result.type == "Text") {
        res_data[result.name]["data"] = job.data;

      } else if (result.type == "File") {
        var fullPath = replaceResultID(result.path, req.params.session);
        fullPath = replaceResultBasePath(fullPath);
        res_data[result.name]["data"] = fullPath;

      } else if (result.type == "TextFile") {
        var fullPath = replaceResultID(result.path, req.params.session);
        fullPath = replaceResultBasePath(fullPath);
        var content = await fs.readFile(fullPath);
        res_data[result.name]["data"] = content.toString('utf8');

      } else if (result.type == "Archive") {
        const zipdir_sync = promisify(zipdir);
        await zipdir_sync(RESULT_PATH, { saveTo: RESULT__ARCHIVE_PATH });
        res_data[result.name]["data"] = RESULT__ARCHIVE_PATH;

      } else {
        res.status(500).json(
          {
            "status": "error",
            "message": "Unknown result type"
          }
        );
      }
    }

    res.json(
      {
        "status": "success",
        "result": res_data
      });
  }
};


function promisify(func) {
  return (...args) =>
    new Promise((resolve, reject) => {
      const callback = (err, data) => err ? reject(err) : resolve(data)

      func.apply(this, [...args, callback])
    })
}