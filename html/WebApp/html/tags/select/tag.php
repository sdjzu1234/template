<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, title, options		***/
	function ct_select($tag)
	{
		$options = explode("|", $tag['attributes']->options);
		array_unshift($options, "Select " . '<div class="sidemenuSubTitle">'.$tag['attributes']->title.'</div>');
		
		$result = '<div class="sidemenuSubTitle">'.$tag['attributes']->title.'</div>';
		
		$result = $result.'<select id="'.$tag['attributes']->id.'" class="dropdownSide">';
		foreach($options as $value) {
			$result = $result.'<option>'.$value.'</option>';
		}
		$result = $result.'</select>';
		
		return $result;
	}
?>