<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="iso-8859-1" />
    <title>SpliceViz on gralline.dinf.fsci.usherbrooke.ca - Splice variants for a family of genes</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/spliceviz.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fabric.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/biojs-io-fasta.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jspdf.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  </head>
  <body>
	<div id="menuoptions">
		<h2>Settings</h2>
		<div>
			<h3>Export figure</h3>
			<button id="exporterjpeg">Export as JPEG</button>
			<button id="exporterpng">Export as PNG</button>
			<button id="exporterpdf">Export as PDF</button>
			<h3>Show/Hide Options</h3>
			<div><input type="checkbox" id="optionsAfficherProperties" checked /> Display properties</div>
			<h3>Genes</h3>
			<div id="optionsListeGenes"></div>
			<button id="optionsAnnuler">Cancel</button>
			<button id="optionsAccepter">Accept</button>
		</div>
	</div>
  </body>
</html>