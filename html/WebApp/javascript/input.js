function inputSubmit()
{
	console.log("input submited");

	var textarea = $("#inputTextArea");

	// validate data type : RNA, tree , ...
	// if RNA
	var dataType = $("#inputTextArea").attr("placeholder");

	if (String(dataType).includes("RNA"))
	{
		var str = textarea.val(); 
		var res = String(str).match(/^>(\w)*(\n)/);
		var res1 = String(res).substring(0, 4);
		var valid = String(str).startsWith(String(res1));

		if(valid) {			
			$.ajax({
			url:'../php/insertRNA.php',
			type: 'POST',
			data: {
				'input': textarea.val()
			},
			success: StandardPhpCallback(updateRNAList),
			error: StandardError()
			});
		}else {
			console.log("bad RNA input");
			// alert("bad input");
			return false;
		}
	}
	else
	{
		$.ajax({
		url:'../php/insertRNA.php',
		type: 'POST',
		data: {
			'input': textarea.val()
		},
		success: StandardPhpCallback(updateRNAList),
		error: StandardError()
		});
	}
}

function inputClear() {

	console.log("input cleared");

	$("#inputTextArea").val("");
}

function inputHide()
{
	$("#inputPanel").hide();
}

function inputShowHide()
{
	var inputPanel = $("#inputPanel")

	if(inputPanel.is(":visible"))
	{
		inputPanel.hide();
	}
	else
	{
		inputPanel.show();
	}
}

function sequenceTypeChanged() {

	var radioValue = $("input[name='SequenceTypeOption']:checked").val();
	if(radioValue){
		console.log("change type of inputTextArea");
		//placeholder="Insert your RNA sequences here."
		$("#inputTextArea").attr("placeholder", "Insert your " + radioValue + " sequences here.");
	}
	
   }