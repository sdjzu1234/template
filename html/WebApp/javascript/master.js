
/* Start of document ready */
$( document ).ready(function() {
	console.log("ready");

	dragElement(document.getElementById("inputPanel"));
	
	$.ajax({
		url:'SideMenu.php',
		type: 'GET',
		success: StandardLoad("menu-content-wrapper"),
		error: StandardError()
		});

//will be moved + callback function added
	$.ajax({
		url:'Input.php',
		type: 'GET',
		success: StandardLoad("input-content-wrapper"),
		error: StandardError()
		});

	updateRNAList();

});
/* End of Document ready */

function updateRNAList() {
	var checkExist = setInterval(function() {
	   if ($('#rnaList-content-wrapper').length) {
		   $.ajax({
				url:'RNAList.php',
				type: 'GET',
				success: StandardLoad("rnaList-content-wrapper"),
				error: StandardError()
			});
			
			clearInterval(checkExist);
		}
	}, 100); // check every 100ms
}

function downloadResult(url){
window.open(url , '_blank');
}

function startAnalysis(){
    
    var inputMode = document.querySelector('input[name="inputmode"]:checked').value;    
    var treenw = document.getElementById("treenw").value;
    var fileUpload = document.getElementById("fileUpload").value;
    var path = "";
    var filename = "";
    var error = "";
    if (inputMode == "modeUpload"){
        var file_data = $('#fileUpload').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);                             
        $.ajax({
            url: 'http://guifette.dinf.fsci.usherbrooke.ca/saveNewickTree.php', 
            dataType: 'json', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'POST',        
                        
            success: function(res){
                console.log(res); 
                if ('path' in res){
                    path = res['path'];
                    filename = res['filename'];
                }else{
                    if ("error" in res){
                       error = res['path'];
                    }else{
                         error = "Server ERROR";
                    }
                }
            }
         });  
    }
    
    if (error != ""){
        alert(error);
        //some treatment for this error    
    }
    else{
         
        var k_indel = document.getElementById("k_indel").value;
        var k_eic = document.getElementById("k_eic").value;
        var k_intron = document.getElementById("k_intron").value;
        var eic_l = document.getElementById("eic_l").value;
        var k_nb_exons = document.getElementById("k_nb_exons").value;
        var eic_g = document.getElementById("eic_g").value;
        var eic_d = document.getElementById("eic_d").value;
        var k_tc = document.getElementById("k_tc").value;
        var tc_tl = document.getElementById("tc_tl").value;
        var tc_a5 = document.getElementById("tc_a5").value;
        var tc_a3 = document.getElementById("tc_a3").value;
        var tc_ek = document.getElementById("tc_ek").value;
        var tc_me = document.getElementById("tc_me").value;
        var tc_ir = document.getElementById("tc_ir").value;
        var tc_rs = document.getElementById("tc_rs").value;
        var e = document.getElementById("numberOfSimulation");
        var numberOfSimulation = e.options[e.selectedIndex].value;                
        var simulationName = document.getElementById("simulationName").value;
        var email = document.getElementById("email").value;

        $.ajax({

                url : 'http://guifette.dinf.fsci.usherbrooke.ca/launchProgram.php',
                type: 'POST',            
                dataType: 'json',
                async: true,
                data: {
                     "inputMode" : inputMode,
                     "path" : path,
                     "filename" : filename,
                     "treenw" : treenw,
                     "k_indel" : k_indel,
                     "k_eic" : k_eic,
                     "k_intron" : k_intron,
                     "eic_l" : eic_l,
                     "k_nb_exons" : k_nb_exons,
                     "eic_g" : eic_g,
                     "eic_d" : eic_d,
                     "k_tc" : k_tc,
                     "tc_tl" : tc_tl,
                     "tc_a5" : tc_a5,
                     "tc_a3" : tc_a3,
                     "tc_ek" : tc_ek,
                     "tc_me" : tc_me,
                     "tc_ir" : tc_ir,
                     "tc_rs" : tc_rs,
                     "numberOfSimulation" : numberOfSimulation,
                     "simulationName" : simulationName,
                     "email" : email,
                },
                success: function (res) {     
                                res = res.replace(/\'/g, "\"");
                                var res = JSON.parse(res);
                                console.log(res['link']);
                                console.log($('#table'));
                                $("#simulationNameVal").text(simulationName);
                                $('#table').bootstrapTable("load", res['iterations']);
                                //$("btnDownload").attr("onclick","downloadResult("+ res['link'] +")");
                                $('#btnDownload').removeAttr('onclick');
                                $('#btnDownload').attr('onClick', 'downloadResult("' + res['link'] + '");');
                                $(function () {
                                    $('#table').bootstrapTable({
                                        data: res['iterations']
                                    });                                                             
                                });                           
                                                            
                                    },
                error: function (e) {
                        console.error("Impossible de lancer l'execution");
                }
        });
    }
}

function drawTree() {
	var analysisType = $("#analType").find(":selected").text();	
	var motifPercentString = $("#analThresh").find(":selected").text();

	var motifPercent = parseInt(motifPercentString);

	var printError = function( req, status, err ) {
	  console.log( 'something went wrong', status, err );
	};
	if(analysisType.localeCompare("Global Analysis")){ //If dropdown = Global Analysis

		$.ajax({
			url:'../php/callPython.php',
			type: 'POST',
			data: {"motifPercent": motifPercent},
	        success: function(id) {
            drawGlobalTree(id);
			$("#overlay").css("display","none");
        	},
			//error: StandardError()
			error: printError
		});
	}
	else{
		$("#errorMessages").append("<p class=\"errorMsg\">Error with the Analysis type selected</p>");
		return; 
	}
}


function inputFileChangedExample()
{
	if(document.getElementById('manual_input').checked) {
		 $("#upload_file").hide();
		 $("#addSeq").show();
	}
 	else if (document.getElementById('upload_input').checked) {
		document.getElementById("upload_file").disabled = false;
		$("#upload_file").show();
		$("#addSeq").hide();
 	}
}

$(function() {
	var open = false;
	$('#footerSlideButton').click(function() {
		if(open === false) {
			$('#footerSlideContent').animate({ height: '300px' });
			$(this).css('backgroundPosition', 'bottom left');
			open = true;
		} else {
			$('#footerSlideContent').animate({ height: '0px' });
			$(this).css('backgroundPosition', 'top left');
			open = false;
		}
	});		
});

