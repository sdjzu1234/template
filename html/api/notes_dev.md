# Description de l'application
Cette application est un template d'API qui permet de gérer l'exécution d'outils du laboratoire COBIUS et de récupérer les résultats de ces exécutions en différé. Pour chaque outil qui va l'utiliser, il faut installer une nouvelle instance de cet API. Tout ce qui est spécifique à l'outil est défini dans le fichier de configuration, par exemple la ligne de commande qui sert à lancer l'outil et les paramètres qui peuvent être ajoutés à cette commande. Pour plus de détails sur l'installation, voir README.md

L'API est donc complètement indépendant du front-end qui va l'utiliser. Pour s'en servir, il faut simplement appeler l'API par des requêtes HTTP.

__Exécution__: L'exécution est lancée par une requête POST dont l'url contient un identifiant de session. Dans le data de la requête, on spécifie les informations qui vont permettre de construire la ligne de commande. Lorsque l'API reçoit cette requête, il construit la commande et l'exécute. Dès l'exécution de la commande, la requête HTTP retourne une réponse contenant la commande qui a été exécutée et l'identifiant de session associé à cette exécution (on n'attend pas la fin de l'exécution). En parallèle, l'API crée une nouvelle «job» dans la base de données et lorsque la commande a fini de s'exécuter, on met à jour cette «job» pour indiquer le résultat de la commande (réussi ou échoué).

__Récupération des résultats__ : Pour récupérer les résultats suite à une exécution, il faut faire une requête de type GET avec l'identifiant de session correspondant à l'exécution qu'on souhaite récupérer. L'API va alors aller chercher la «job» associée à cet identifiant de session dans la base de données. Si l'exécution n'est pas encore complétée, la réponse de la requête HTTP contiendra le statut «pending». Si une erreur est survenue lors de l'exécution, la requête HTTP va retourner un statut «error» avec une explication de l'erreur, si possible. Finalement, si l'exécution s'est complétée avec succès, on va retourner le statut «success» avec les données de résultat. C'est le fichier de configuration de l'outil qui détermine quelles informations doivent être retournées (par exemple la sortie stdout de la commande, un ou plusieurs fichiers, une archive contenant plusieurs résultats).


# Structure du code
## Racine
_app.js_ : Initialisation de l'application. Ce fichier contient la connection à la base de données MongoDB, l'instanciation des routes et le démarrage du serveur sur le port spécifié

_package.json_ : Spécification du projet NodeJS (voir la documentation de npm)

## Config
_database.json_ : Informations de configuration de la base de données MongoDB. (incluant le nom de la base de données, le nom d'utilisateur et le mot de passe pour y accéder)

_tool.json_ : Fichier de configuration de l'outil. Voir le README.md pour le format complet

## Controllers
_tool.js_ : Contient la logique qui est exécutée lors des appels HTTP pour lancer une exécution ou récupérer les résulats

## Models
_job.js_ : Schéma de la collection Jobs de la base de données MongoDB. Chaque « job » correspond à une exécution de l'outil

## Routers
_tools.js_ : Fait le lien entre les urls des requêtes HTTP et le code de contrôleur à exécuter


# Installation actuelle sur le serveur http://bioinfo.dinf.usherbrooke.ca
Le serveur NodeJS de l'API est actuellement installé dans le répertoire /srv/api du serveur http://bioinfo.dinf.usherbrooke.ca pour l'outil SpliceFamViz. 
Il devrait être démarré en permanence, mais ce n'est pas toujours le cas actuellement. Il faudrait possiblement créer un script qui le lancerait automatiquement au redémarrage du serveur.
(Voir le fichier README.md pour les instructions sur comment le démarrer)
Si on veut instancier d'autres versions de l'api pour d'autres outils, on pourra rajouter un répertoire api2 dans /srv. (Voir le fichier README.md pour les instructions sur comment installer une nouvelle instance de l'api)

_NOTE_ : Actuellement, l'API peut seulement être appelé à l'intérieur même du serveur (via l'adresse localhost:3001). C'est probablement dû à une configuration du serveur. En attendant de savoir comment lui permettre d'être accessible de l'extérieur (via l'adresse http://bioinfo.dinf.usherbrooke.ca/...), on peut utiliser un « pont » fait en PHP. Pour un exemple, voir la branche ApiIntegration du projet SpliceFamViz sur le dépot GitLab.

# Pistes de résolution de problèmes
1. S'assurer que l'application NodeJS roule toujours (en vérifiant que le port 3001 est en cours d'utilisation)
2. S'assurer que le service de MongoDB est toujours en cours d'exécution (en vérifiant que le port 27017 est en cours d'utilisation)
3. Vérifier qu'il n'y a pas eu d'erreur qui a pu faire planter l'application NodeJS. Si c'est le cas, simplement redémarrer l'application (`npm start` dans le répertoire /srv/api)

Commande pour tester que un port est ouvert:
nmap -p num_port server_ip_address

Commande pour ouvrir port
sudo ufw allow num_port

Commande pour afficher liste des ports actifs
sudo netstat -tulpn

# Étapes restantes
- Ajouter une validation sur l'accès à la base de données au cas où l'entrée n'est pas trouvée (ex. si on accède à un identifiant de session inexistant) pour éviter que l'application plante. api/controllers/tools.js l.223
- Nettoyer périodiquement les données sur le serveur pour éviter de manquer d'espace. Ce pourrait être un script qu'on exécute manuellement ou bien qui roule de façon automatisée. Ce script supprimerait les répertoires _app/inputs/[id]_ et _app/results/[id]_ pour les identifiants qui ont été créés depuis X temps, soit en regardant la date d'exécution pour cet id dans la base de données ou en se fiant à la date de création du répertoire.
