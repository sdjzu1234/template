#!/usr/bin/python3.5
#-*- coding: utf-8 -*-

"""

``statsSimSpliceEvol.py`` **module description**:

This module ....

.. moduleauthor:: Aida Ouangraoua

2019

"""
import sys
import glob, os
from Bio.Data import CodonTable
from Bio import SeqIO
from Bio import Seq
from Bio.Seq import Seq


def  cluster(file):
    lines = open(file,"r").readlines()
    i = 0
    nb_class = 0.0
    nb_cds = 0.0
    while i < len(lines):
        if(lines[i].startswith('>')):
            nb_class +=1
            i+=1
            parse = lines[i].split("\n")[0].split(", ")
            nb_cds += len(parse)
        i+=1

    return nb_cds/nb_class, nb_cds, nb_class


def format_seq_aln(seq1,seq2):
    aln = []
    i = 0
    j = 0
    
    for k in range(len(seq1)):
        if(seq1[k] != '-' and seq2[k] != '-'):
            aln.append((i,j))
        if(seq1[k] != '-'):
            i += 1
        if(seq2[k] != '-'):
            j += 1
    return aln
    
def compute_sequence_identity(seq1,seq2):
    
    match = 0
    length = 0
    for i in range(len(seq1)):
        if(seq1[i] != '-' or seq2[i] != '-'):
            length += 1
            if(seq1[i] == seq2[i]):
                match += 1
    if length ==0:
	    return 'Error'
    return 1.0*match/length


def computeStat(SimulatedDir):
    result = {}
    result["iterations"] = []
    for file in glob.glob(SimulatedDir+ "/positions/*"):
        if "~" in file:
            continue

        benchmarkfile = file.split("\n")[0].split("/")[-1]
        prefix  = benchmarkfile.split("exon_positions.fasta")[0]

        benchorthologyfile = SimulatedDir+ "/cluster/"+prefix+"cds_gene.txt"
        benchgenefile = SimulatedDir+"/genes/"+prefix+"gene.fasta"
        benchcdsfile = SimulatedDir+"/cds/"+prefix+"cds.fasta"
        benchalnfile = SimulatedDir+"/multiple_alignment/"+prefix+"multiple.fasta"

        clussterinfos, nb_cds, nb_class= cluster(SimulatedDir+"/cluster/"+prefix+"cluster.fasta")


        alnids = []
        alnsequences = []
        for record in SeqIO.parse(benchalnfile, "fasta"):
            alnids.append(record.id)
            alnsequences.append(record.seq)
        nb_seq = len(alnsequences)
        pid = 0.0
        nb = 0.0
        benchaln = {}
        for i in range(nb_seq):	
            for j in range(i+1, nb_seq):
                pid_cur = compute_sequence_identity(alnsequences[i],alnsequences[j])
                if pid_cur != 'Error' and (pid_cur != 1.0 and alnids[i].split("_")[1] != alnids[j].split("_")[1]) :
                    pid += pid_cur
                    nb += 1
                    benchaln[alnids[i]+"_"+alnids[j]] = format_seq_aln(alnsequences[i],alnsequences[j])
                else:
                    pass
        if nb !=0:
             pid = pid/nb

        nb_cds = 0
        nb_gene = 0
        cds_len = 0.0
        gene_len = 0.0
        
        for record in SeqIO.parse(benchcdsfile, "fasta"):
            cds_seq = str(record.seq)
            nb_cds += 1
            cds_len += len(cds_seq)
        cds_len = cds_len/nb_cds

        for record in SeqIO.parse(benchgenefile, "fasta"):
            gene_seq = str(record.seq)
            nb_gene += 1
            gene_len += len(gene_seq)
        gene_len = gene_len/nb_gene
        
        mean_nb_cds_per_gene = (1.0*nb_cds)/nb_gene    
        iteration = prefix.split("_")[2]

        result["iterations"].append({"iteration":iteration, "PID": round(pid, 2), "number_gene":round(nb_gene, 2), "number_cds":round(nb_cds,2), "mean_nb_cds_per_gene":round(mean_nb_cds_per_gene,2), 'number_cluster': round(nb_class,2), 'number_cds_per_cluster': round(clussterinfos,2), "avg_gene_length":round(gene_len,2),  "avg_cds_length":round(cds_len,2)})
    
    nb_iteration = 0
    pid = 0.0
    nb_gene = 0.0
    nb_cds = 0.0
    mean_nb_cds_per_gene = 0.0
    nb_cluster = 0.0
    nb_cds_per_cluster = 0.0
    avg_gene_len = 0.0
    avg_cds_len = 0.0
    
    for values in result["iterations"]:
        nb_iteration += 1
        pid += float(values["PID"])
        nb_gene += values["number_gene"]
        nb_cds += values["number_cds"]
        mean_nb_cds_per_gene += values["mean_nb_cds_per_gene"]
        nb_cluster += values["number_cluster"]
        nb_cds_per_cluster += values["number_cds_per_cluster"]
        avg_gene_len += values["avg_gene_length"]
        avg_cds_len += values["avg_cds_length"]
        
    pid /= nb_iteration
    nb_gene /= nb_iteration
    nb_cds /= nb_iteration
    mean_nb_cds_per_gene /= nb_iteration
    nb_cluster /= nb_iteration
    nb_cds_per_cluster /= nb_iteration
    avg_gene_len /= nb_iteration
    avg_cds_len /= nb_iteration
    
    result["summary"] = {"PID": round(pid,2), "number_gene":round(nb_gene,2), "number_cds":round(nb_cds,2), "mean_nb_cds_per_gene":round(mean_nb_cds_per_gene,2), 'number_cluster': round(nb_cluster,2), 'number_cds_per_cluster': round(nb_cds_per_cluster,2), "avg_gene_length":round(avg_gene_len,2),  "avg_cds_length":round(avg_cds_len,2)}
    result["iterations"].append({"iteration":"summary", "PID": round(pid,2), "number_gene":round(nb_gene,2), "number_cds":round(nb_cds,2), "mean_nb_cds_per_gene":round(mean_nb_cds_per_gene,2), 'number_cluster': round(nb_cluster,2), 'number_cds_per_cluster': round(nb_cds_per_cluster,2), "avg_gene_length":round(avg_gene_len,2),  "avg_cds_length":round(avg_cds_len,2)})
    return result 

