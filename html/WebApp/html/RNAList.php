<?php
	require_once('../php/RNA.class.php');

	$id = session_id();
	session_start();
	if(isset($_SESSION['rnaList']))
	{
		$rnaList = $_SESSION['rnaList'];
	}
	else
	{
		$rnaList = array();
	}

	if (empty($_SESSION['count'])) {
   $_SESSION['count'] = 1;
} else {
   $_SESSION['count']++;
}

?>

<div id="rnaList">
	<ul>
		<?php
			foreach ($rnaList as $rna) 
			{				
				$name = explode("|",$rna->header)[0]; 
				echo $name;
		?>
				<li id="arnlist<?php echo $rna->id; ?>" onclick="switchListSelection(<?php echo $rna->id; ?>)" class="rnaUnselected"><?php echo $name; ?> </li>
		<?php	
			}
		?>
	</ul>
</div>
