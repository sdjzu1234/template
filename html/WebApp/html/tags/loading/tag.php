<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, img_src 			***/
	function ct_loading($tag)
	{
		return '
		<div id="'.$tag['attributes']->id.'" class="loading">
			<img id="loaderGif" src="'.$tag['attributes']->img_src.'" />
		</div>';
	}
?>