
<?php

function validNewickTree($filename){
    $file_parts = pathinfo($filename);
    if ($file_parts['extension'] == "nw"){
        return True;
    }
    else{
        return False;
    }
}

function tempdir() {
    $name = 'SimSpliceEvol_' . uniqid();
    $dir =  '/var/www/applications/SimSpliceEvol/Example/onlineRunning/' . $name . '/';
       
    if(!file_exists($dir)) {
        if(mkdir($dir, 0777) === true) {
	        chmod($dir,0777);
            return $dir;
        }
    }
    return tempdir();
}

$dir = tempdir();
$filename = $_FILES['file']['name'];
if (validNewickTree($filename)){
    try {
        if ( 0 < $_FILES['file']['error'] ) {
            echo json_encode(array('error' => 'Error: ' . $_FILES['file']['error'] . '<br>'));
        }
        else {
            move_uploaded_file($_FILES['file']['tmp_name'], $dir . $filename);
            echo json_encode(array('path' => $dir, "filename" => $filename));
        }
    } catch (Exception $e) {
        echo json_encode(array('error' => $e->getMessage()));
    }
}
else{
    echo json_encode(array('error' => "Your input file should be newick file ending by .nw"));
}

?>

