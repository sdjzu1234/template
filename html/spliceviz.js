///|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///*******************************************************************************************************
/// Samuel Whittom, Juin - Août 2017
///*******************************************************************************************************
///|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
function spliceviz(canvas) {
	// Define a default sequence object
	var positionSequenceXmin = 170;
	var positionSequenceXmax = 1110;
	var facteurEtirementExons = 1; // 1 = 1px par position ex: 0-96 prend 96px sur l'écran
	var longueurMultiblocs = [];
	var debutMultiblocs = [];
	var debutMultiblocspx = [];
	var hauteurExons = 15;
	var optionsPropertiesVisible = true;
	var colorProperties = "#ff0000";
	var colorExonMissingFromTranscripts = "#777777";
	var colorExonPresentInTranscripts = "#000000";
	var colorExonTranscript = ["#1B2845", "#5899e2", "#706993"];
	var strTranscript2Gene = "";
	var strFastaGene = "";
	var strFastaTranscript = "";
	var strExonList = "";
	var strMacroalignment = "";
	var strMicroalignment = "";
	var linkGtfProperties = "https://raw.githubusercontent.com/samdouble/bioinfo/master/entrees/fam86.gtf";

	// This function gives uniform properties to all lines drawn on the canvas
	function formatFabricObject(obj) {
		obj.setOptions({
			stroke: 'black',
			hasControls: false,
			lockMovementX: true,
			lockMovementY: true,
			lockScalingX: true,
			lockScalingY: true,
			lockRotation: true,
			hoverCursor: 'pointer'
		});
		obj.setControlsVisibility({ mt: false, mb: false, ml: false, mr: false, bl: false, br: false, tl: false, tr: false, mtr: false });
	}
	// This function determines the properties shown when the user clicks on the object
	function setClickableFabricObject(obj, exonmicro) {
		obj.id = exonmicro.id;
		obj.appartienta = exonmicro.appartienta;
		obj.familleparent = exonmicro.familleparent;
		if (exonmicro.appartienta == "gene") {
			obj.geneparent = exonmicro.geneparent;
			obj.missingfromtranscripts = exonmicro.missingfromtranscripts;
		}
		else if (exonmicro.appartienta == "transcrit") {
			obj.geneparent = exonmicro.geneparent;
			obj.transcritparent = exonmicro.transcritparent;
		}
		obj.multibloc = exonmicro.multibloc;
		obj.debut = exonmicro.debut;
	}

	var sequence = {
		exonsmicro: [],
		nom: "",
		positiony: 0,
		type: "famille",
		dessiner: function () {
			canvas.add(new fabric.Text(this.nom, {
				fontFamily: 'Delicious_500',
				fontSize: 14,
				left: 20,
				top: this.positiony,
				hasControls: false,
				lockMovementX: true,
				lockMovementY: true,
				hoverCursor: 'pointer'
			}));
			// Si la séquence commence par un intron
			if (this.exonsmicro.length > 0 && this.exonsmicro[0].debutpx > positionSequenceXmin) {
				var ligne = new fabric.Line([0, 0, this.exonsmicro[0].debutpx - positionSequenceXmin, 0], {
					strokeWidth: 2,
					left: positionSequenceXmin,
					top: this.positiony + 10,
					width: this.exonsmicro[0].debutpx - positionSequenceXmin
				});
				formatFabricObject(ligne);
				canvas.add(ligne);
				ligne.setCoords();
			}
			for (var i = 0; i < this.exonsmicro.length; i++) {
				// Exon
				var objExon;
				if (this.exonsmicro[i].type == "plein") {
					objExon = new fabric.Rect({
						height: hauteurExons,
						fill: this.exonsmicro[i].couleur,
						strokeWidth: 1,
						left: this.exonsmicro[i].debutpx,
						top: this.positiony,
						width: this.exonsmicro[i].longueurpx
					});

				}
				else if (this.exonsmicro[i].type == "vide") {
					objExon = new fabric.Line([0, 0, this.exonsmicro[i].longueurpx, 0], {
						strokeDashArray: [2, 2],
						strokeWidth: 2,
						left: this.exonsmicro[i].debutpx,
						top: this.positiony + 10,
						width: this.exonsmicro[i].longueurpx
					});
				}
				formatFabricObject(objExon);
				setClickableFabricObject(objExon, this.exonsmicro[i]);
				canvas.add(objExon);
				objExon.setCoords();

				// Intron suivant
				var largeurIntron;
				if (i == this.exonsmicro.length - 1)
					largeurIntron = positionSequenceXmax - (this.exonsmicro[i].debutpx + this.exonsmicro[i].longueurpx);
				else
					largeurIntron = this.exonsmicro[i + 1].debutpx - (this.exonsmicro[i].debutpx + this.exonsmicro[i].longueurpx);
				if (largeurIntron != 0) {
					if (this.type === "famille" || this.type === "gene") {
						var rect = new fabric.Rect({
							height: hauteurExons,
							fill: 'white',
							strokeWidth: 1,
							left: this.exonsmicro[i].debutpx + this.exonsmicro[i].longueurpx,
							top: this.positiony,
							width: largeurIntron
						});
						formatFabricObject(rect);
						canvas.add(rect);
						rect.setCoords();
					} else {
						var ligne = new fabric.Line([0, 0, largeurIntron, 0], {
							strokeWidth: 2,
							left: this.exonsmicro[i].debutpx + this.exonsmicro[i].longueurpx,
							top: this.positiony + 10,
							width: largeurIntron
						});
						formatFabricObject(ligne);
						canvas.add(ligne);
						ligne.setCoords();
					}
				}
			}
			return;
		}
	}

	function arrayContient(array, champ, valeur) {
		if (typeof valeur == "string") {
			for (var i = 0; i < array.length; i++)
				if (array[i][champ].trim() == valeur.trim())
					return i;
		} else {
			for (var i = 0; i < array.length; i++)
				if (array[i][champ] == valeur)
					return i;
		}
		return -1;
	}

	function getLongueurMaximaleTotalExons(genes) {
		var somme = 0;
		for (var i = 0; i < longueurMultiblocs.length; i++) {
			somme += longueurMultiblocs[i];
		}
		return somme;
	}

	function getNbMultiblocs(genes) {
		return genes[0].exons.length;
	}

	function getLongueurIntrons(nbMultiblocs, longueurTotaleExons) {
		var x = positionSequenceXmax - positionSequenceXmin;
		facteurEtirementExons = (0.9 * x) / (longueurTotaleExons + 5 * nbMultiblocs);
		return Math.round((x - facteurEtirementExons * longueurTotaleExons) / nbMultiblocs);
	}

	function getExonsMicroalignes(exonmacro, seqmicro, debutMultiblocs, debutMultiblocspx, longueurMultiblocs, listeFinsExons) {
		var debut;
		var fin;
		var exons = [];
		var longueurAttendueExon = exonmacro.fin - exonmacro.debut;
		var exonid = makeExonId(8);
		// On cherche le premier caractère qui n'est pas un -
		var i = 0;
		while (seqmicro[debutMultiblocs[exonmacro.multibloc] + i] == "-")
			i++;

		for (; i < longueurMultiblocs[exonmacro.multibloc];) {
			debut = i;
			// On cherche la fin de l'exon plein
			while (i < longueurMultiblocs[exonmacro.multibloc] && seqmicro[debutMultiblocs[exonmacro.multibloc] + i] != "-")
				i++;
			fin = i;

			var exon = {};
			exon.id = exonid;
			exon.type = "plein";
			exon.appartienta = exonmacro.appartienta;
			exon.familleparent = famille.nom;
			exon.geneparent = exonmacro.geneparent;
			if (exonmacro.appartienta == "transcrit")
				exon.transcritparent = exonmacro.transcritparent;
			exon.multibloc = exonmacro.multibloc;
			exon.debut = exonmacro.debut + debut;
			exon.debutcontigu = debutMultiblocs[exonmacro.multibloc] + debut;
			exon.debutpx = exonmacro.debutpx + Math.round(facteurEtirementExons * debut);
			exon.fin = exonmacro.debut + fin;
			exon.fincontigu = debutMultiblocs[exonmacro.multibloc] + fin;
			exon.longueurpx = Math.round(facteurEtirementExons * (fin - debut));
			exon.couleur = exonmacro.couleur;
			exons.push(exon);

			// Si on a tout l'exon à ce moment, inutile d'aller plus loin
			longueurAttendueExon -= (fin - debut);
			if (longueurAttendueExon == 0)
				break;

			// Sinon, on cherche l'exon "vide"
			debut = i;
			while (i < longueurMultiblocs[exonmacro.multibloc] && seqmicro[debutMultiblocs[exonmacro.multibloc] + i] == "-")
				i++;
			// On ne peut pas finir par un exon vide dans un multibloc puisque les exons vides doivent obligatoirement être entre 2 exons pleins
			if (i >= longueurMultiblocs[exonmacro.multibloc])
				break;
			fin = i;
			// On détermine si c'est un exon "vide" ou un intron. Si c'est un exon de transcrit et qu'il se trouve dans la liste de fin des exons, c'est un intron et on ne l'ajoute pas
			if (listeFinsExons == null || listeFinsExons.indexOf(exon.fin.toString()) == -1) {
				var exon = {};
				exon.id = exonid;
				exon.type = "vide";
				exon.appartienta = exonmacro.appartienta;
				exon.familleparent = famille.nom;
				exon.geneparent = exonmacro.geneparent;
				if (exonmacro.appartienta == "transcrit")
					exon.transcritparent = exonmacro.transcritparent;
				exon.multibloc = exonmacro.multibloc;
				exon.debut = exonmacro.debut + debut;
				exon.debutcontigu = debutMultiblocs[exonmacro.multibloc] + debut;
				exon.debutpx = exonmacro.debutpx + Math.round(facteurEtirementExons * debut);
				exon.fin = exonmacro.debut + fin;
				exon.fincontigu = debutMultiblocs[exonmacro.multibloc] + fin;
				exon.longueurpx = Math.round(facteurEtirementExons * (fin - debut));
				exon.couleur = exonmacro.couleur;
				exons.push(exon);
			} else if (listeFinsExons.indexOf(exon.fin.toString()) != -1) {
				// on doit générer des ids différents pour des exons différents au sein d'un même multibloc
				exonid = makeExonId(8);
			}
		}
		return exons;
	}

	function dessinerFigure() {
		canvas.clear();
		canvas.backgroundColor = "white";
		var famille01 = sequence;
		famille01.type = "famille";
		famille01.nom = famille.nom;
		famille01.exonsmicro = famille.exons;
		famille01.positiony = 20;
		famille01.dessiner();

		var y = 70;
		// On place les gènes dans l'ordre pour les afficher
		var genesOrdonnes = genes.sort(function (a, b) {
			return b.ordre < a.ordre;
		});
		for (var i = 0; i < genesOrdonnes.length; i++) {
			if (genesOrdonnes[i].visible) {
				var gene = sequence;
				gene.type = "gene";
				gene.nom = genesOrdonnes[i].nom;
				gene.exonsmicro = genesOrdonnes[i].exonsmicro;
				gene.positiony = y;
				gene.dessiner();
				y += 40;

				// Draw the gene's properties, if there are any
				if (optionsPropertiesVisible && genesOrdonnes[i].properties != null) {
					var property = sequence;
					property.type = "properties";
					property.nom = "";
					property.exonsmicro = genesOrdonnes[i].properties;
					property.positiony = y;
					property.dessiner();
					y += 40;
				}

				// Draw the gene's transcripts
				for (var j = 0; j < genesOrdonnes[i].transcrits.length; j++) {
					var transcrit = sequence;
					transcrit.type = "transcrit";
					transcrit.nom = genesOrdonnes[i].transcrits[j].nom;
					transcrit.exonsmicro = genesOrdonnes[i].transcrits[j].exonsmicro;
					transcrit.positiony = y;
					transcrit.dessiner();
					y += 40;

					// Draw the transcript's properties, if there are any
					if (optionsPropertiesVisible && genesOrdonnes[i].transcrits[j].properties != null) {
						var property = sequence;
						property.type = "properties";
						property.nom = "";
						property.exonsmicro = genesOrdonnes[i].transcrits[j].properties;
						property.positiony = y;
						property.dessiner();
						y += 40;
					}
				}
			}
		}
	}

	function getOrdreGenes() {
		var ordre = [];
		$('#optionsListeGenes div input').each(function (e) {
			ordre.push($(this).attr('id').split("_")[1].trim());
		});
		return ordre;
	}

	function execute() {
		$.ajax({
			url: 'http://bioinfo.dinf.usherbrooke.ca:3001/outil/00001',
			type: 'POST',
			dataType: 'json',
			data: {
				"input_type": true,
				"source_file": strFastaTranscript,
				"target_file": strFastaGene,
				"source2target_file": strTranscript2Gene,
				"sourceexonlist_file" : strExonList,
				"output_format": "list"
			},
			success: function (res) {
				var getCall = () => $.ajax({
					url: 'http://bioinfo.dinf.usherbrooke.ca:3001/outil/00001',
					type: 'GET',
					data: {},
					success: function (res) {
						if (res.status == "pending") {
							setTimeout(getCall, 1000);
						} else if (res.status == "success") {
							console.log(res.result);
							strMacroalignment = res.result.macroalignment.data;
							strMicroalignment = res.result.microalignment.data;

						} else {
							console.error("Impossible de récupérer le résultat");
						}
					},
					error: function (err) {
						console.error("Impossible de récupérer le résultat");
						console.log(err);
					}
				});

				getCall();




			},
			error: function () {
				console.error("Impossible de lancer l'exécution");
			}
		});
	}

	var famille;
	var genes;
	function computeFigure() {
		famille = { nom: "FAM86" };
		genes = [];
		allText = strTranscript2Gene;
		var lignes = allText.split("\n");
		var ordreGenes = 0;
		for (var i = 0; i < lignes.length; i++) {
			var transcritGene = lignes[i].split(" ");
			var indexGene = arrayContient(genes, "nom", transcritGene[1]);
			if (indexGene == -1) // Nouveau gène trouvé
			{
				var gene = {};
				gene.nom = transcritGene[1];
				gene.exons = [];
				gene.visible = true;
				gene.ordre = ordreGenes;
				var transcrit = {};
				transcrit.nom = transcritGene[0];
				transcrit.exons = [];
				gene.transcrits = [transcrit];
				genes.push(gene);
				ordreGenes++;
			} else { // Gène déjà existant, on ajoute le transcrit à la liste de transcrits de ce gène
				var transcrit = {};
				transcrit.nom = transcritGene[0];
				transcrit.exons = [];
				genes[indexGene].transcrits.push(transcrit);
			}
		}
		////////////////////////////////////////////////////////////
		/// Ajout des séquences pour les gènes
		////////////////////////////////////////////////////////////
		var Fasta = require('biojs-io-fasta');
		var modelgene = Fasta.parse(strFastaGene);
		/*Fasta.read(linkFastaGene, function(err, modelgene) {*/
		for (var i = 0; i < genes.length; i++) {
			var indexGene = arrayContient(modelgene, "name", genes[i].nom);
			if (indexGene == -1)
				console.error("The file gene.fasta does not contain a sequence for the gene " + genes[i].nom);
			else
				genes[i].seq = modelgene[indexGene].seq;
		}
		/*});*/
		////////////////////////////////////////////////////////////
		/// Ajout des séquences pour les transcrits
		////////////////////////////////////////////////////////////
		/*Fasta.read(linkFastaTranscript, function(err, model) {*/
		var model = Fasta.parse(strFastaTranscript);
		for (var i = 0; i < genes.length; i++) {
			for (var j = 0; j < genes[i].transcrits.length; j++) {
				var indexTranscrit = arrayContient(model, "name", genes[i].transcrits[j].nom);
				if (indexTranscrit == -1)
					console.error("The file transcrit.fasta does not contain a sequence for the transcript " + genes[i].transcrits[j].nom);
				else
					genes[i].transcrits[j].seq = model[indexTranscrit].seq;
			}
		}
		/*});*/
		////////////////////////////////////////////////////////////
		/// Macroalignements
		////////////////////////////////////////////////////////////
		var macroLignes = strMacroalignment.split("\n");
		var multiBloc = -1;
		var plusLongExonDuMultiBloc = 0;
		var regexpSeq = /^([A-Za-z0-9]+):(\d+)\-(\d+)/;
		for (var i = 0; i < macroLignes.length; i++) {
			if (macroLignes[i].charAt(0) == '>') {
				multiBloc++;
				plusLongExonDuMultiBloc = 0;
			} else {
				var match = regexpSeq.exec(macroLignes[i]);
				if (match != null) {
					var exon = {};
					exon.id = makeExonId(8);
					exon.multibloc = multiBloc;
					exon.debut = parseInt(match[2]);
					exon.fin = parseInt(match[3]);
					exon.couleur = "#000000"; // Pour les transcrits, ça pourrait être modifié plus tard

					// Si l'exon est de longueur 0, inutile de l'ajouter
					if (exon.debut != exon.fin) {
						// Est-ce un exon de gène?
						var in_genes = arrayContient(genes, "nom", match[1]);
						if (in_genes == -1) {
							// Non, c'est un exon de transcrit
							for (var j = 0; j < genes.length; j++) {
								var in_transcrits = arrayContient(genes[j].transcrits, "nom", match[1]);
								if (in_transcrits != -1) {
									exon.appartienta = "transcrit";
									exon.familleparent = famille.nom;
									exon.geneparent = j;
									exon.transcritparent = in_transcrits;
									genes[j].transcrits[in_transcrits].exons.push(exon);
								}
							}
						} else {
							exon.appartienta = "gene";
							exon.familleparent = famille.nom;
							exon.geneparent = in_genes;
							genes[in_genes].exons.push(exon);
							// Si c'est la plus longue séquence pour ce multibloc, on l'indique
							if (longueurMultiblocs[multiBloc] == null || (exon.fin - exon.debut) > longueurMultiblocs[multiBloc])
								longueurMultiblocs[multiBloc] = exon.fin - exon.debut;
						}
					}
				}
			}
		}
		////////////////////////////////////////////////////////////
		/// Calcul de la position en pixels des exons
		////////////////////////////////////////////////////////////
		var longueurTotaleExons = getLongueurMaximaleTotalExons(genes);
		var longueurIntrons = getLongueurIntrons(getNbMultiblocs(genes), longueurTotaleExons);

		// On calcule le début en position et en pixels de chacun des multiblocs
		debutMultiblocs[0] = 0;
		debutMultiblocspx[0] = positionSequenceXmin;
		for (var i = 1; i < longueurMultiblocs.length; i++) {
			debutMultiblocs[i] = debutMultiblocs[i - 1] + longueurMultiblocs[i - 1];
			debutMultiblocspx[i] = debutMultiblocspx[i - 1] + Math.round(facteurEtirementExons * longueurMultiblocs[i - 1]) + longueurIntrons;
		}

		// On calcule le début-fin de chacun des exons de la famille en pixels
		famille.exons = [];
		for (var i = 0; i < debutMultiblocspx.length; i++) {
			famille.exons[i] = {
				id: makeExonId(8),
				appartienta: "famille",
				familleparent: famille.nom,
				multibloc: i,
				debut: debutMultiblocs[i],
				debutpx: debutMultiblocspx[i],
				longueurpx: Math.round(facteurEtirementExons * longueurMultiblocs[i]),
				type: "plein",
				couleur: "#000000"
			};
		}

		// On calcule le début-fin de chacun des exons des gènes et des transcrits en pixels
		for (var i = 0; i < genes.length; i++) {
			for (var k = 0; k < genes[i].exons.length; k++) {
				genes[i].exons[k].debutpx = debutMultiblocspx[genes[i].exons[k].multibloc];
				genes[i].exons[k].longueurpx = Math.round(facteurEtirementExons * (genes[i].exons[k].fin - genes[i].exons[k].debut));
			}
			for (var j = 0; j < genes[i].transcrits.length; j++) {
				for (var k = 0; k < genes[i].transcrits[j].exons.length; k++) {
					genes[i].transcrits[j].exons[k].debutpx = debutMultiblocspx[genes[i].transcrits[j].exons[k].multibloc];
					genes[i].transcrits[j].exons[k].longueurpx = Math.round(facteurEtirementExons * (genes[i].transcrits[j].exons[k].fin - genes[i].transcrits[j].exons[k].debut));
				}
			}
		}

		////////////////////////////////////////////////////////////
		/// Calcul des couleurs des exons de transcrits
		////////////////////////////////////////////////////////////
		var couleursMultiBlocs = [];
		for (var i = 0; i < genes.length; i++) {
			for (var j = 0; j < genes[i].transcrits.length; j++) {
				for (var k = 0; k < genes[i].transcrits[j].exons.length; k++) {
					// Si c'est le premier exon du multibloc qu'on rencontre, on créé l'index
					if (couleursMultiBlocs[genes[i].transcrits[j].exons[k].multibloc] == null)
						couleursMultiBlocs[genes[i].transcrits[j].exons[k].multibloc] = [];
					// Si la couleur n'est pas déjà définie pour ce modulo et ce multibloc, on la définit dans couleursMultiBlocs
					if (couleursMultiBlocs[genes[i].transcrits[j].exons[k].multibloc][(genes[i].transcrits[j].exons[k].debut % 3)] == null) {
						couleursMultiBlocs[genes[i].transcrits[j].exons[k].multibloc][(genes[i].transcrits[j].exons[k].debut % 3)] = colorExonTranscript[couleursMultiBlocs[genes[i].transcrits[j].exons[k].multibloc].length];
					}
					// Assigner la couleur à l'exon pour le multibloc auquel appartient l'exon
					genes[i].transcrits[j].exons[k].couleur = couleursMultiBlocs[genes[i].transcrits[j].exons[k].multibloc][(genes[i].transcrits[j].exons[k].debut % 3)];
				}
			}
		}
		////////////////////////////////////////////////////////////
		/// CDSEXONSLIST
		////////////////////////////////////////////////////////////
		Fasta.parse(strExonList, function (err, cdsexonslist) {
			////////////////////////////////////////////////////////////
			/// Microalignements
			////////////////////////////////////////////////////////////
			Fasta.parse(strMicroalignment, function (err, modelmicro) {
				for (var i = 0; i < modelmicro.length; i++) {
					// Est-ce un exon de gène?
					var in_genes = arrayContient(genes, "nom", modelmicro[i].name);
					if (in_genes == -1) {
						// Non, c'est un exon de transcrit
						for (var j = 0; j < genes.length; j++) {
							var in_transcrits = arrayContient(genes[j].transcrits, "nom", modelmicro[i].name);
							if (in_transcrits != -1)
								genes[j].transcrits[in_transcrits].seqmicro = modelmicro[i].seq;
						}
					} else {
						genes[in_genes].seqmicro = modelmicro[i].seq;
					}
				}
				// Recalcul des exons (exonsmicro) en tenant compte des microalignements
				for (var i = 0; i < genes.length; i++) {
					// On doit calculer les exons microalignés des transcrits avant les exons de leur gène parent
					for (var j = 0; j < genes[i].transcrits.length; j++) {
						// On va chercher les fins des exons de ce transcrit, qui ont été lues plus tôt dans cdsexonslist
						var listefinsexons;
						var indexTranscrit = arrayContient(cdsexonslist, "name", genes[i].transcrits[j].nom);
						if (indexTranscrit != -1) {
							listefinsexons = cdsexonslist[indexTranscrit].seq.trim().split(" ");
						} else {
							console.error("Le fichier cdsexonslist.txt n\'est pas défini pour le transcrit " + genes[i].transcrits[j].nom);
						}

						// On construit les exonsmicro à partir des exons et de la liste des fins des exons
						genes[i].transcrits[j].exonsmicro = [];
						for (var k = 0; k < genes[i].transcrits[j].exons.length; k++) {
							genes[i].transcrits[j].exonsmicro = genes[i].transcrits[j].exonsmicro.concat(getExonsMicroalignes(genes[i].transcrits[j].exons[k], genes[i].transcrits[j].seqmicro, debutMultiblocs, debutMultiblocspx, longueurMultiblocs, listefinsexons));
						}
					}

					// Calcul des exons microalignés des gènes
					genes[i].exonsmicro = [];
					for (var k = 0; k < genes[i].exons.length; k++) {
						genes[i].exonsmicro = genes[i].exonsmicro.concat(getExonsMicroalignes(genes[i].exons[k], genes[i].seqmicro, debutMultiblocs, debutMultiblocspx, longueurMultiblocs, null));
					}

					////////////////////////////////////////////////////////////
					/// Calcul de la couleur des exons de gènes
					/// Gris si aucun de ses transcrits n'a la séquence correspondante
					/// Noir si au moins un de ses transcrits a la séquence correspondante
					/// On se fie à la position (en px) des exons microalignés pour ce calcul
					////////////////////////////////////////////////////////////
					var exonsmicroGenesCouleur = [];
					for (var k = 0; k < genes[i].exonsmicro.length; k++) {
						if (genes[i].exonsmicro[k].type == "plein") {
							var arr = "";
							for (var m = genes[i].exonsmicro[k].debutcontigu; m < genes[i].exonsmicro[k].fincontigu; m++) {
								var dansUnDesTranscrits = false;
								for (var j = 0; j < genes[i].transcrits.length; j++) {
									if (genes[i].transcrits[j].seqmicro[m] != "-") {
										dansUnDesTranscrits = true;
										break;
									}
								}
								arr += dansUnDesTranscrits ? "1" : "0";
							}

							for (m = 0; m < arr.length; m++) {
								var debut = m;
								while (arr[m] == "1")
									m++;
								var fin = m;

								var exon = {};
								exon.id = genes[i].exonsmicro[k].id;
								exon.type = "plein";
								exon.missingfromtranscripts = false;
								exon.appartienta = genes[i].exonsmicro[k].appartienta;
								exon.familleparent = famille.nom;
								exon.geneparent = genes[i].exonsmicro[k].geneparent;
								if (genes[i].exonsmicro[k].appartienta == "transcrit")
									exon.transcritparent = genes[i].exonsmicro[k].transcritparent;
								exon.multibloc = genes[i].exonsmicro[k].multibloc;
								exon.debut = genes[i].exonsmicro[k].debut + debut;
								exon.debutcontigu = genes[i].exonsmicro[k].debutcontigu + debut;
								exon.debutpx = genes[i].exonsmicro[k].debutpx + Math.round(facteurEtirementExons * debut);
								exon.fin = genes[i].exonsmicro[k].debut + fin;
								exon.fincontigu = genes[i].exonsmicro[k].debutcontigu + fin;
								exon.longueurpx = Math.round(facteurEtirementExons * (fin - debut));
								exon.couleur = colorExonPresentInTranscripts;
								if (exon.debut != exon.fin)
									exonsmicroGenesCouleur.push(exon);
								//****************************************************
								var debut = m;
								while (arr[m] == "0")
									m++;
								var fin = m;

								var exon = {};
								exon.id = genes[i].exonsmicro[k].id;
								exon.type = "plein";
								exon.missingfromtranscripts = true;
								exon.appartienta = genes[i].exonsmicro[k].appartienta;
								exon.familleparent = famille.nom;
								exon.geneparent = genes[i].exonsmicro[k].geneparent;
								if (genes[i].exonsmicro[k].appartienta == "transcrit")
									exon.transcritparent = genes[i].exonsmicro[k].transcritparent;
								exon.multibloc = genes[i].exonsmicro[k].multibloc;
								exon.debut = genes[i].exonsmicro[k].debut + debut;
								exon.debutcontigu = genes[i].exonsmicro[k].debutcontigu + debut;
								exon.debutpx = genes[i].exonsmicro[k].debutpx + Math.round(facteurEtirementExons * debut);
								exon.fin = genes[i].exonsmicro[k].debut + fin;
								exon.fincontigu = genes[i].exonsmicro[k].debutcontigu + fin;
								exon.longueurpx = Math.round(facteurEtirementExons * (fin - debut));
								exon.couleur = colorExonMissingFromTranscripts;
								if (exon.debut != exon.fin)
									exonsmicroGenesCouleur.push(exon);
							}
						}
						else if (genes[i].exonsmicro[k].type == "vide") {
							exonsmicroGenesCouleur.push(genes[i].exonsmicro[k]);
						}
					}
					genes[i].exonsmicro = exonsmicroGenesCouleur;
				}

				////////////////////////////////////////////////////////////
				/// Properties
				////////////////////////////////////////////////////////////
				var gtffile = new XMLHttpRequest();
				gtffile.open("GET", linkGtfProperties, true);
				gtffile.onreadystatechange = function () {
					if (gtffile.readyState === 4 && gtffile.status === 200) {
						var gtfLignes = gtffile.responseText.split("\n");
						var regexpGtf = /^\d+\s+\w+\s+(\w+)\s+(\d+)\s+(\d+).+gene_id\s"(\w+)/;
						for (var i = 0; i < gtfLignes.length; i++) {
							var match = gtfLignes[i].match(regexpGtf);
							if (match != null) {
								var objproperty = {
									debut: parseInt(match[2]),
									debutpx: positionSequenceXmin + Math.round(facteurEtirementExons * parseInt(match[2])),
									fin: parseInt(match[3]),
									longueurpx: Math.round(facteurEtirementExons * (parseInt(match[3]) - parseInt(match[2]))),
									type: "plein",
									couleur: colorProperties
								};
								var indexGene = arrayContient(genes, "nom", match[4]);
								if (match[1] == "gene") {
									if (indexGene != -1) {
										if (genes[indexGene].properties == null)
											genes[indexGene].properties = [];
										genes[indexGene].properties.push(objproperty);
									}
								} else if (match[1] == "transcript") {
									var regexpGtfTrans = /transcript_id\s"(\w+)/;
									var matchTrans = gtfLignes[i].match(regexpGtfTrans);
									var indexTranscrit = arrayContient(genes[indexGene].transcrits, "nom", matchTrans[1]);
									if (indexTranscrit != -1) {
										if (genes[indexGene].transcrits[indexTranscrit].properties == null)
											genes[indexGene].transcrits[indexTranscrit].properties = [];
										genes[indexGene].transcrits[indexTranscrit].properties.push(objproperty);
									}
								}
							}
						}

						////////////////////////////////////////////////////////////
						/// Figure
						////////////////////////////////////////////////////////////
						dessinerFigure();
					}
				}
				gtffile.send(null);
			});
		});


		console.log(genes);
		////////////////////////////////////////////////////////////
		/// Options
		////////////////////////////////////////////////////////////
		$(function () {
			genes.forEach(function (el) {
				$("#optionsListeGenes").append("<div><input type=\"checkbox\" class=\"optionsAfficherGene\" id=\"optionsAfficherGene_" + el.nom + "\" " + (el.visible ? "checked" : "") + " /> " + el.nom);
			});
			// Pouvoir réordonner les gènes
			$("#optionsListeGenes").sortable({
				update: function (event, ui) { }
			});
		});
		/*}
	}
	txtFile.send(null);*/
	}

	//computeFigure();

	////////////////////////////////////////////////////////////
	/// Exporter
	////////////////////////////////////////////////////////////
	$('#exporterpng').click(function () {
		var img = canvas.toDataURL("image/png");
		window.open(img, "_blank");
	});
	$('#exporterjpeg').click(function () {
		var img = canvas.toDataURL({
			format: 'jpeg',
			quality: 0.92
		});
		window.open(img, "_blank");
	});
	$('#exporterpdf').click(function () {
		var imgData = canvas.toDataURL("image/jpeg", 1.0);
		var pdf = new jsPDF('l', 'mm', [297, 210]);

		pdf.addImage(imgData, 'JPEG', 0, 0);
		var download = document.getElementById('download');

		pdf.save("download.pdf");
	});

	////////////////////////////////////////////////////////////
	/// Menu contextuel et commandes
	////////////////////////////////////////////////////////////
	var sourisEnfoncee = false;
	var endroitEnfonce = [0, 0];
	function showContextMenu(x, y) {
		$('#menucontextuel').css("display", "block");
		$('#menucontextuel').css('left', x + "px");
		$('#menucontextuel').css('top', y + "px");
	}
	// Get all gene exons that are part of the specified multiblock
	function getGeneExonsInFamily(genes, multiblock) {
		arr = [];
		for (var i = 0; i < genes.length; i++) {
			for (var k = 0; k < genes[i].exonsmicro.length; k++) {
				if (genes[i].exonsmicro[k].multibloc == multiblock) {
					if (arrayContient(arr, "id", genes[i].exonsmicro[k].id) == -1) {
						var geneExon = $.extend(true, {}, genes[i].exonsmicro[k]);
						var startEnd = getExonStartEnd(genes[i], genes[i].exonsmicro[k].id);
						geneExon.debutcontigu = startEnd.start;
						geneExon.fincontigu = startEnd.end;
						arr.push(geneExon);
					}
				}
			}
		}
		return arr;
	}
	// Get all transcript exons that are part of the specified multiblock
	function getTranscriptExonsInGene(gene, multiblock) {
		arr = [];
		for (var i = 0; i < gene.transcrits.length; i++) {
			for (var k = 0; k < gene.transcrits[i].exonsmicro.length; k++) {
				if (gene.transcrits[i].exonsmicro[k].multibloc == multiblock) {
					if (arrayContient(arr, "id", gene.transcrits[i].exonsmicro[k].id) == -1) {
						var transcriptExon = $.extend(true, {}, gene.transcrits[i].exonsmicro[k]);
						var startEnd = getExonStartEnd(gene.transcrits[i], gene.transcrits[i].exonsmicro[k].id);
						transcriptExon.debutcontigu = startEnd.start;
						transcriptExon.fincontigu = startEnd.end;
						arr.push(transcriptExon);
					}
				}
			}
		}
		return arr;
	}

	function getExonStartEnd(splice, idExon) {
		var startEnd = { start: Infinity, end: 0 };
		for (var k = 0; k < splice.exonsmicro.length; k++) {
			if (idExon == splice.exonsmicro[k].id) {
				startEnd.start = Math.min(startEnd.start, splice.exonsmicro[k].debutcontigu);
				startEnd.end = Math.max(startEnd.end, splice.exonsmicro[k].fincontigu);
			}
		}
		return startEnd;
	}

	// Same as getExonStartEnd, except for a gene, we take into account whether this part of the exon is part of the gene's transcripts or not
	function getExonStartEndWithMissingFromTranscripts(splice, idExon, missingFromTranscripts) {
		var startEnd = { start: Infinity, end: 0 };
		for (var k = 0; k < splice.exonsmicro.length; k++) {
			if (idExon == splice.exonsmicro[k].id && splice.exonsmicro[k].missingfromtranscripts == missingFromTranscripts) {
				startEnd.start = Math.min(startEnd.start, splice.exonsmicro[k].debutcontigu);
				startEnd.end = Math.max(startEnd.end, splice.exonsmicro[k].fincontigu);
			}
		}
		return startEnd;
	}

	canvas.observe('mouse:down', function (e) {
		if (e.target == null) {
			sourisEnfoncee = true;
		}
		endroitEnfonce = [e.e.layerX, e.e.layerY];
		hideAllMenus();
	});
	canvas.observe('mouse:move', function (e) {
		if (sourisEnfoncee) {
			canvas.relativePan(new fabric.Point(e.e.layerX - endroitEnfonce[0], e.e.layerY - endroitEnfonce[1]));
			endroitEnfonce = [e.e.layerX, e.e.layerY];
		}
	});
	canvas.observe('mouse:up', function (e) {
		sourisEnfoncee = false;
		// Si c'est un drag ou un clic
		if (Math.abs(e.e.layerX - endroitEnfonce[0]) <= 1 && Math.abs(e.e.layerY - endroitEnfonce[1]) <= 1) {
			if (e.target != null && e.target.get("familleparent") != null) {
				$("#menucontextuel ul").empty();
				$("#menucontextuel ul").append("<li><b>HGNC_trans_name:</b> " + e.target.get("familleparent") + "</li>");
				var downloadContents = "";
				// A family exon was clicked
				if (e.target.get("appartienta") == "famille") {
					var geneExons = getGeneExonsInFamily(genes, e.target.get("multibloc"));
					for (var i = 0; i < geneExons.length; i++) {
						var gene = genes[geneExons[i].geneparent];
						$("#menucontextuel ul").append("<li>" + geneExons[i].id + " " + gene.nom + "</li>");

						downloadContents += "> " + geneExons[i].id + " " + gene.nom + "\n";
						downloadContents += gene.seqmicro.substring(geneExons[i].debutcontigu, geneExons[i].fincontigu);
						downloadContents += "\n\n";

						var transcriptExons = getTranscriptExonsInGene(gene, e.target.get("multibloc"));
						for (var j = 0; j < transcriptExons.length; j++) {
							var transcrit = gene.transcrits[transcriptExons[j].transcritparent];
							$("#menucontextuel ul").append("<li>" + transcriptExons[j].id + " " + transcrit.nom + "</li>");

							downloadContents += "> " + transcriptExons[j].id + " " + transcrit.nom + " " + gene.nom + "\n";
							downloadContents += transcrit.seqmicro.substring(transcriptExons[j].debutcontigu, transcriptExons[j].fincontigu);
							downloadContents += "\n\n";
						}
					}
				}
				// A gene exon was clicked
				else if (e.target.get("appartienta") == "gene") {
					var clickedGene = genes[e.target.get("geneparent")];
					$("#menucontextuel ul").append("<li>" + e.target.get("id") + " " + clickedGene.nom + "</li>");
					if (e.target.get("missingfromtranscripts")) {
						var clickedExon = clickedGene.exonsmicro[arrayContient(clickedGene.exonsmicro, "debut", e.target.get("debut"))];// On se fie au début plutôt qu'à l'id pour identifier seulement la zone grise
						var startEndObj = getExonStartEndWithMissingFromTranscripts(clickedGene, e.target.get("id"), true);
						downloadContents += "> " + e.target.get("id") + " " + clickedGene.nom + "\n";
						downloadContents += clickedGene.seqmicro.substring(startEndObj.start, startEndObj.end);
					} else {
						var startEndObj = getExonStartEndWithMissingFromTranscripts(clickedGene, e.target.get("id"), false);
						downloadContents += "> " + e.target.get("id") + " " + clickedGene.nom + "\n";
						downloadContents += clickedGene.seqmicro.substring(startEndObj.start, startEndObj.end);
						downloadContents += "\n\n";

						var transcriptExons = getTranscriptExonsInGene(clickedGene, e.target.get("multibloc"));
						for (var j = 0; j < transcriptExons.length; j++) {
							var transcrit = clickedGene.transcrits[transcriptExons[j].transcritparent];
							$("#menucontextuel ul").append("<li>" + transcriptExons[j].id + " " + transcrit.nom + "</li>");

							downloadContents += "> " + transcriptExons[j].id + " " + transcrit.nom + " " + clickedGene.nom + "\n";
							downloadContents += transcrit.seqmicro.substring(transcriptExons[j].debutcontigu, transcriptExons[j].fincontigu);
							downloadContents += "\n\n";
						}
					}
				}
				// A transcript exon was clicked
				else if (e.target.get("appartienta") == "transcrit") {
					var clickedGene = genes[e.target.get("geneparent")];
					var clickedTranscript = clickedGene.transcrits[e.target.get("transcritparent")];
					var startEndObj = getExonStartEnd(clickedTranscript, e.target.get("id"));

					$("#menucontextuel ul").append("<li>" + e.target.get("id") + " " + clickedTranscript.nom + "</li>");

					downloadContents += "> " + e.target.get("id") + " " + clickedTranscript.nom + " " + clickedGene.nom + "\n";
					downloadContents += clickedTranscript.seqmicro.substring(startEndObj.start, startEndObj.end);
				}
				$("#menucontextuel ul").append("<li><a href=\"#\" class=\"telechargerFichierFasta\">Download</a></li>");
				showContextMenu(e.e.layerX, e.e.layerY);
				// File download
				$(".telechargerFichierFasta").click(function () {
					download("spliceviz_" + Date.now() + ".fasta", downloadContents);
				});
			}
		} else {
			canvas.relativePan(new fabric.Point(e.e.layerX - endroitEnfonce[0], e.e.layerY - endroitEnfonce[1]));
		}
	});
	function zoom(e) {
		$('#menucontextuel').css("display", "none");

		var evt = window.event || e;
		var delta = evt.detail ? evt.detail * (-120) : evt.wheelDelta;
		var curZoom = canvas.getZoom(), newZoom = curZoom + delta / 4000,
			x = e.offsetX, y = e.offsetY;
		canvas.zoomToPoint({ x: x, y: y }, newZoom);
		if (e != null) e.preventDefault();
		return false;
	}
	var applyZoom = function () {
		var canvasarea = document.getElementById("canvasarea");
		canvasarea.addEventListener("mousewheel", function (e) { zoom(e); }, false);
		canvasarea.addEventListener("DOMMouseScroll", function (e) { zoom(e); }, false);
	}
	applyZoom();

	////////////////////////////////////////////////////////////
	/// Options
	////////////////////////////////////////////////////////////
	function hideAllMenus() {
		$('#menucontextuel').css("display", "none");
		$('#menuoptions').css("display", "none");
		$('#menuupload').css("display", "none");
	}
	function openMenu(idMenu) {
		if ($(idMenu).css("display") == "none") {
			var hauteurMenu = $(idMenu).height();
			var largeurMenu = $(idMenu).width();
			$(idMenu).css({ display: "block", top: canvas._offset.top + (canvas.height - hauteurMenu) / 2, left: canvas._offset.left + (canvas.width - largeurMenu) / 2 });
		} else {
			$(idMenu).css("display", "none");
		}
	}

	$("#iconeupload").css({ top: canvas._offset.top + canvas.height - 150, left: canvas._offset.left + canvas.width - 50 });
	$("#iconereset").css({ top: canvas._offset.top + canvas.height - 100, left: canvas._offset.left + canvas.width - 50 });
	$("#iconeoptions").css({ top: canvas._offset.top + canvas.height - 50, left: canvas._offset.left + canvas.width - 50 });
	$("#iconeupload").click(function () {
		hideAllMenus();
		openMenu("#menuupload");
	});
	$("#iconereset").click(function () {
		canvas.setZoom(1);
		canvas.relativePan(new fabric.Point(-canvas.viewportTransform[4], -canvas.viewportTransform[5]));
	});
	$("#iconeoptions").click(function () {
		hideAllMenus();
		openMenu("#menuoptions");
	});
	$("#optionsAnnuler").click(function () {
		$('#menuoptions').css("display", "none");
	});
	$("#uploadAnnuler").click(function () {
		$('#menuupload').css("display", "none");
	});
	$("#optionsAccepter").click(function () {
		// Visibilité des propriétés
		optionsPropertiesVisible = $("#optionsAfficherProperties").is(':checked');

		// Visibilité des gènes
		$(".optionsAfficherGene").each(function () {
			var index = arrayContient(genes, "nom", $(this).attr("id").split("_")[1]);
			if (index != -1) {
				genes[index].visible = $(this).is(':checked');
			}
		});
		// Ordre des gènes
		var ordreGenes = getOrdreGenes();
		for (var i = 0; i < genes.length; i++) {
			var ordre = ordreGenes.indexOf(genes[i].nom.trim());
			if (ordre != -1)
				genes[i].ordre = ordre;
		}
		hideAllMenus();
		dessinerFigure();
	});

	$("#computeForm").submit(function (e) {
		console.log("submit form");
		e.preventDefault();
		strTranscript2Gene = $('#files_t2gtxt').val();
		strFastaGene = $('#files_gfasta').val();
		strFastaTranscript = $('#files_tfasta').val();
		strExonList = $('#files_exon').val();
		$("#result").show();
		$("#input_data").hide();
		execute();
		computeFigure();
		hideAllMenus();
	});

	////////////////////////////////////////////////////////////
	/// File download
	////////////////////////////////////////////////////////////
	function download(filename, text) {
		var element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		element.setAttribute('download', filename);

		element.style.display = 'none';
		document.body.appendChild(element);

		element.click();

		document.body.removeChild(element);
	}

	////////////////////////////////////////////////////////////
	/// Generates a random string of a given length
	////////////////////////////////////////////////////////////
	function makeExonId(length) {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < length; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	}
}
