#!/bin/bash
#$1 -n 
#$2 -i 
#$3 -k_indel
#$4 -k_eic 
#$5 -k_intron
#$6 -eic_l 
#$7 -k_nb_exons
#$8 -eic_g 
#$9 -eic_d 
#$10 -k_tc 
#$11 -tc_tl 
#$12 -tc_a5 
#$13 -tc_a3 
#$14 -tc_ek 
#$15 -tc_me 
#$16 -tc_ir 
#$17 -tc_rs 
#pip3.5 install --user numpy
/usr/bin/python3.5 /var/www/applications/SimSpliceEvol/src/SimSpliceEvol.py -n $1 -i $2 -k_indel $3 -k_eic $4 -k_intron $5 -eic_l $6 -k_nb_exons $7 -eic_g $8 -eic_d $9 -k_tc ${10} -tc_tl ${11} -tc_a5 ${12} -tc_a3 ${13} -tc_ek ${14} -tc_me ${15} -tc_ir ${16} -tc_rs ${17}
