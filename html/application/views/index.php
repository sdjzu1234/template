<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="iso-8859-1" />
    <title>SpliceViz on gralline.dinf.fsci.usherbrooke.ca - Splice variants for a family of genes</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap337.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/spliceviz.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap337.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fabric.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/biojs-io-fasta.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jspdf.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<style type="text/css">
	.erreurs{
		padding:15px;
		background-color: #ffaaaa;
		border: 1px solid red;
	}
	</style>
  </head>
  <body>
	<div class="container">
		<h2>SpliceViz</h2>
		<form method="post" action="<?php echo base_url(); ?>run">
			<p>Please select the files that will be used to calculate and draw the figure.</p>
			<?php if(isset($check1Echoue) && $check1Echoue){ ?>
			<p class="erreurs"><?php foreach ($erreurs as $erreur){ echo $erreur . "<br />"; } ?></p>
			<?php } ?>
			<div class="form-group">
				<label for="files_gfasta">Gene.fasta</label><br />
				<textarea id="files_gfasta" name="files_gfasta" rows="6" cols="70"><?php if(isset($input)){ echo $input['files_gfasta']; } ?></textarea>
			</div>
			<div class="form-group">
				<label for="files_tfasta">Transcript.fasta</label><br />
				<textarea id="files_tfasta" name="files_tfasta" rows="6" cols="70"><?php if(isset($input)){ echo $input['files_tfasta']; } ?></textarea>
			</div>
			<div class="form-group">
				<label for="files_t2gtxt">Transcript2gene.txt</label><br />
				<textarea id="files_t2gtxt" name="files_t2gtxt" rows="6" cols="70"><?php if(isset($input)){ echo $input['files_t2gtxt']; } ?></textarea>
			</div>
			<input type="hidden" name="changeInput" value="0" />
			<input type="submit" value="Run" />
			<button id="resetFields" type="button">Reset</button>
		</form>
		<script type="text/javascript">
			$("#resetFields").click(function(){
				$("#files_gfasta").val("");
				$("#files_tfasta").val("");
				$("#files_t2gtxt").val("");
			});
		</script>
	</div>
  </body>
</html>