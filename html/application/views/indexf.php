<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NHL stats</title>

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<style>
	table {
		border-collapse: collapse;
	}

	table, th, td {
		border: 1px solid black;
	}
	th, td {
		padding: 15px;
		text-align: left;
	}
	th {
		background-color: #4CAF50;
		color: white;
	}
	</style>
</head>
<body>
    <div id="nhl_code" class="col-sm-12 text-left padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms" style="font-size:1.1em;"></div>
	<script type="text/javascript" src="http://localhost/nhl/jquery-1.8.min.js"></script>
	<script type="text/javascript">
	</script>
</body>
</html>