<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, img_src 			***/
	function ct_checkbox($tag)
	{
		$options_number_checkboxBtn = '';
		$options_checkboxBtn = '';
		$options_checkboxBtn_text = '';
		$result = '';

		/*more than one btn checkbox at the time*/
		if (property_exists($tag['attributes'], 'number'))
		{
			$index = 0;
			$options_number_checkboxBtn = $tag['attributes']->number;
			$options_checkboxBtn_value = explode("|", $tag['attributes']->value);
			$options_checkboxBtn_text = explode("|", $tag['attributes']->text);

			foreach ($options_checkboxBtn_value as $value) {
				$result = $result.'<input type="checkbox" id='.$tag['attributes']->id.' name='.$tag['attributes']->name.' value='.$value.'> '.$options_checkboxBtn_text[$index].'<br>';
				$index++;
			}
			return $result;

		}
		
		// one checkbox button return
		$result = '<input type="checkbox" id='.$tag['attributes']->id.' name='.$tag['attributes']->name.' value='.$tag['attributes']->value.'> '.$tag['attributes']->text.'<br>';
		
		return $result;
	}
?>