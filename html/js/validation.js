$( document ).ready(function() {
	$("#option1").hide();
	$("#option2").hide();
	$("#option3").hide();
	$("#option4").hide();
	//var result =  $("#result").hide();
    $("#selectInputType").click(function( event ) {
        var inputType = $("#selectInputType option:selected").val();

        if (inputType == "option0"){
        	$("#option1").hide();
        	$("#option2").hide();
        	$("#option3").hide();
        	$("#option4").hide();
        }
        if (inputType == "option1"){
        	$("#option1").show();
        	$("#option2").hide();
        	$("#option3").hide();
        	$("#option4").hide();
        }
        if (inputType == "option2"){
        	$("#option1").hide();
        	$("#option2").show();
        	$("#option3").hide();
        	$("#option4").hide();
        }
        if (inputType == "option3"){
        	$("#option1").hide();
        	$("#option2").hide();
        	$("#option3").show();
        	$("#option4").hide();
        }
        if (inputType == "option4"){
        	$("#option1").hide();
        	$("#option2").hide();
        	$("#option3").hide();
        	$("#option4").show();
        }
    });

});
