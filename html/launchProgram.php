
<?php

function tempdir() {
    $name = 'SimSpliceEvol_' . uniqid();
    $dir =  '/var/www/applications/SimSpliceEvol/Example/onlineRunning/' . $name . '/';
       
    if(!file_exists($dir)) {
        if(mkdir($dir, 0777) === true) {
	        chmod($dir,0777);
            return $dir;
        }
    }
    return tempdir();
}

$inputMode=$_POST["inputMode"];
$Treefilename=$_POST["filename"];
$path=$_POST["path"];
$treenw=$_POST["treenw"];
$k_indel=$_POST["k_indel"];
$k_eic=$_POST["k_eic"];
$k_intron=$_POST["k_intron"];
$eic_l=$_POST["eic_l"];
$k_nb_exons=$_POST["k_nb_exons"];
$eic_g=$_POST["eic_g"];
$eic_d=$_POST["eic_d"];
$k_tc=$_POST["k_tc"];
$tc_tl=$_POST["tc_tl"];
$tc_a5=$_POST["tc_a5"];
$tc_a3=$_POST["tc_a3"];
$tc_ek=$_POST["tc_ek"];
$tc_me=$_POST["tc_me"];
$tc_ir=$_POST["tc_ir"];
$tc_rs=$_POST["tc_rs"];
$numberOfSimulation=$_POST["numberOfSimulation"];
$simulationName=$_POST["simulationName"];
$email=$_POST["email"];


try {
    if ($inputMode == "modeUpload" and $path!= ""){
        //user upload a file tree and its already save on the server
        $guitreTreeFile = $path . $Treefilename;
    }else{
        $dir = tempdir();
        $guitreTreeFile = $dir . "guideTree.nw";
        $myfile = fopen($guitreTreeFile, "w") or die("Unable to open file!");
        fwrite($myfile, $treenw);
        fclose($myfile);    
    }
    //chdir('/var/www/applications/SimSpliceEvol/src/');
    // $params = ' -n '.$numberOfSimulation .' -i '.$guitreTreeFile. ' -k_indel '.$k_indel.' -k_eic '.$k_eic.' -k_intron '.$k_intron.' -eic_l '. $eic_l.' -k_nb_exons '.$k_nb_exons.' -eic_g '.$eic_g.' -eic_d '.$eic_d.' -k_tc '.$k_tc.' -tc_tl '.$tc_tl.' -tc_a5 '.$tc_a5.' -tc_a3 '.$tc_a3.' -tc_ek '.$tc_ek.' -tc_me '.$tc_me.' -tc_ir '.$tc_ir.' -tc_rs '.$tc_rs;

    //$output = shell_exec(escapeshellcmd("python3.5 /var/www/applications/SimSpliceEvol/src/SimSpliceEvol.py ".  $params . " 2>&1"));
    
    $output = shell_exec('/var/www/applications/SimSpliceEvol/src/launch.sh '. $numberOfSimulation .' '.  $guitreTreeFile .' '. $k_indel .' '. $k_eic .' '. $k_intron .' '. $eic_l .' '. $k_nb_exons .' '. $eic_g .' '. $eic_d .' '. $k_tc .' '. $tc_tl .' '. $tc_a5 .' '. $tc_a3 .' '. $tc_ek .' '. $tc_me .' '. $tc_ir .' '. $tc_rs . '  2>&1');
    
} catch(Exception $e) {
    json_encode(array('error',  $e->getMessage(), "\n"));
}

echo json_encode($output);
?>

