# CobiusWEB

Structure de projet permettant de rendre accessible un outil du laboratoire CoBIUS à partir du web.

- [CobiusWEB](#cobiusweb)
    - [Installation et configuration](#installation-et-configuration)
        - [1. Application web](#1-application-web)
            - [a) PHP-Custom-Tags](#a-php-custom-tags)
                - [1. Dossier HTML](#1-dossier-html)
                - [2. Dossier Tags](#2-dossier-tags)
        - [2. API](#2-api)
            - [a) Installation des dépendances](#a-installation-des-d%C3%A9pendances)
                - [1. Installation de NodeJS](#1-installation-de-nodejs)
                - [2. Installation des librairies NPM](#2-installation-des-librairies-npm)
                - [3. Installation de MongoDB](#3-installation-de-mongodb)
                - [4. Création de la base de données](#4-cr%C3%A9ation-de-la-base-de-donn%C3%A9es)
                - [5. Choix du port (facultatif)](#5-choix-du-port-facultatif)
            - [b) Configuration de l'outil](#b-configuration-de-loutil)
        - [3. Application (outil)](#3-application-outil)
    - [Démarrage](#d%C3%A9marrage)
        - [1. Application web](#1-application-web)
        - [2. API](#2-api)
    - [Utilisation](#utilisation)
    - [Exemples](#exemples)
        - [Exemple d'un fichier de configuration (tool.json)](#exemple-dun-fichier-de-configuration-tooljson)


## Installation et configuration 
__*Pour le développeur de l'application*__

_Pour créer une nouvelle instance du projet, il faut d'abord copier l'entièreté de ce projet sur le serveur où l'application sera exécutée.
Les étapes suivantes expliqueront comment adapter chacune des parties du projet pour l'application souhaitée._

### 1. Application web

Ici, nous décrivons le contenu du répertoire /srv/html
#### a) PHP-Custom-Tags
Nous utilisons ici un API qui permet de créé des balises HTML personallisable et réutilisable.

Pour l'instant vous pouvez voir un exemple sur RNAFamViz (Dossier [WebApp](WebApp/)), pour le futur il serais pertinant de l'installer également sur SpliceFamViz...

Pour des informations plus approfondit, voir [le README de l'outil.](https://github.com/buggedcom/PHP-Custom-Tags/blob/master/README.md)

##### 1. Dossier HTML
_*Exemple d'utilisation: [SideMenu.php](WebApp/html/SideMenu.php)*_

Pour chaque page ou vous voulez utiliser des custom-tags. Vous n'avez qu'à insérer une balise php comme dans l'exemple.

Par la suite les balises définies dans le dossier [tags](WebApp/html/tags/) peuvent être utiliser avec \<ct:*NomBalise* *att1*="*val1*" *att2*="*val2*" ... >

##### 2. Dossier Tags
_*Exemple d'utilisation: [tag.php](WebApp/html/tags/button/tag.php)*_

Pour faire des balises personalisable, on a qu'à suivre la structure de ce dossier.

Dans (WebApp/html/tags)[Webapp/html/tags/] on a des dossiers avec le nom des balises que nous voulons faire;
si on veut créer une nouvelle balise, on a qu'à créer un nouveau dossier avec le nom voulu.

Dans un dossier, on doit ajouter un fichier nommé *tag.php*, comme dans l'exemple.
Dans ce fichier, on  doit créé une fonction nommé *ct_"nomBalise"*, voir l'exemple.
Cette fonction doit retourner un string, qui construit la balise à l'aide de des balises html simple.
Cette fonction doit retourner avoir un attribut $tag, qui contient des propriété de la balise.
Il est possible de référencer une autre balise personalisable.
Il est possible également de référencer des attributs, à l'aide de la variable $tag.

### 2. API

#### o) Copie des fichiers

Copier le contenu de CoBIUSWeb/api (gitlab) dans /srv/api

On suppose que /srv contient également les répertoires api, app et html

#### a) Installation des dépendances
Les dépendances requises pour l'API sont :
- NodeJS avec npm
- MongoDB

##### 1. Installation de NodeJS
```
sudo apt-get update
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```
##### 2. Installation des librairies NPM
```
sudo apt-get install npm
npm install
```
##### 3. Installation de MongoDB
Voir ce tutoriel : https://hevodata.com/blog/install-mongodb-on-ubuntu/

##### 4. Création de la base de données
_La base de données sert à conserver les informations sur les exécutions (jobs) des outils._

- Démarrer le shell mongo avec la commande `mongo -u admin -p admin --authenticationDatabase admin`. (Utiliser le nom d'utilisateur et le mot de passe choisis à l'étape 3)
- Une fois dans le shell mongo, exécuter les commandes suivantes: 
```
use cobius
db.jobs.insert({"test" : "test"})
db.jobs.remove({"test" : "test"})
db.createUser({user:"admin", pwd:"admin", roles:[{role:"root",db:"admin"}]})  // User et password au choix
```
_Pour qu'une base de données soit créée dans MongoDB, il faut au moins insérer un document dedans. C'est pourquoi on insère le document "test" et qu'on le supprime tout de suite après._

Pour valider que la création a fonctionné, vous pouvez lister la liste des bases de données avec `show dbs` dans le shell mongo. Il devrait y avoir _cobius_ dans la liste.

- Mettre à jour le fichier `cobius_web/api/config/database.json` avec le user et le password choisis précédemment

##### 5. Choix du port (facultatif)
Si plusieurs instances du projet sont installées sur la même machine (pour des outils différents, par exemple), il faudra modifier le numéro de port dans le fichier `cobius_web/api/app.js l.5`. N'importe quel port libre devrait fonctionner.



#### b) Configuration de l'outil
_La configuration de l'outil est un fichier de format JSON. Celui-ci se trouve à l'emplacement : `/src/api/config/tool.json`. Voici le format de configuration. Sauf s'il est indiqué *optionnel*, tous les champs ci-dessous sont obligatoires pour que la configuration fonctionne correctement._

- `name` : Nom de l'outil (Utile seulement pour repérer plus rapidement duquel il s'agit, pas utilisé dans le code)

- `base_command` : La commande à exécuter (sans les paramètres). Peut être un script shell, python ou tout autre exécutable (à partir du répertoire /srv/app).

- `params` : La listes des paramètres qui peuvent être ajoutés à la commande de base. Chaque paramètre est construit de cette façon :
    - `name` : Nom du paramètre. Le nom est également utilisé dans l'application web pour indiquer à quel champ du formulaire il correspond, il est donc important que les deux utilisent le même nom.

    - `prefix` : Préfixe du paramètre qui sera ajouté avant la valeur de celui-ci. Pour y inclure **l'identifiant de session**, mettre `{{id}}`, la véritable valeur sera remplacée dynamiquement au moment de construire la ligne de commande.

    - `value_type` : Type de valeur du paramètre. Valeurs possibles:
        * None: Aucune valeur provenant de l'utilisateur, seul le préfixe sera ajouté
        * String: Valeur textuelle 
        * Int: Nombre entier 
        * Float: Nombre flottant
        * File: Chemin vers un fichier. L'api va recevoir les données que doit contenir le fichier, écrire le fichier puis indiquer le chemin vers le fichier comme paramètre dans la commande

    - `file_name` _optionnel_: Seulement si la valeur est de type *File*, le nom ou chemin du fichier à utiliser (relatif au répertoire `cobius_web/app/inputs/{{id}}/`)

    - `optionnal` _optionnel_: Indique si le paramètre est optionnel pour la commande. Si le champ est absent de la configuration, le paramètre est obligatoire par défaut.

- `results` : La liste des résultats à retourner à l'application web. Chaque résultat est construit de cette façon : 
     - `name` : Nom de l'élément. (Utile afin de pouvoir l'identifier lors de l'affichage ou du traitement des données par l'application web)
     - `type` : Type de données à retourner. Valeurs possibles:
        * Archive : Un fichier zip regroupant toutes les données du répertoire `results`
        * File : Le chemin vers un fichier spécifique de `results`
        * FileText : Le contenu en texte d'un fichier spécifique de `results`
        * Text : Le texte retourné dans la console par la commande

    - `path` _optionnel_: Seulement pour les types File et FileText, chemin vers le fichier à retourner (relatif au répertoire `cobius_web/app/results/{{id}}/`)


### 3. Application (outil)

Ici, on décrit le contenu du répertoire /srv/app

1. Installer toutes les dépendances nécessaires pour votre application
2. Installer l'application dans le répertoire `srv/app/` avec un script shell pour le lancement*(normalement il suffit de copier-coller les fichiers et répertoires de l'application, à moins que l'outil nécessite une installation plus complexe)*


## Démarrage
__*Pour le développeur de l'application*__
L'application web ainsi que l'API devraient être actifs en tout temps. Voici les instructions pour les démarrer la première fois ou pour les redémarrer en cas d'arrêt du serveur, d'erreur ou de tout autre problème.


### 1. Application web

*TODO*


### 2. API
1. Exécuter les commandes ci-dessous afin de s'assurer que le service MongoDB est actif :
   ```
   sudo service mongod start
   mongod
   ```
2. Exécuter la commande ci-dessous afin de s'assurer que les dépendances NodeJS sont à jour :
   ```
   npm install
   ```
3. À partir du répertoire `cobius_web/api`, lancer la commande suivante :
   ```
   npm start
   ```


## Utilisation
__*Pour l'utilisateur de l'application*__

*TODO*


## Exemples
### Exemple d'un fichier de configuration (tool.json)
```JSON
{
    "name": "SpliceFamViz",
    "base_command": "../SpliceFamAlignMulti/launch_splicefamalign_without_exonlist.sh",
    "params": [
        {
            "name": "source_file",
            "prefix": "",
            "value_type": "File",
            "file_name": "transcripts.fasta",
            "order": 1
        },
        {
            "name": "target_file",
            "prefix": "",
            "value_type": "File",
            "file_name": "genes.fasta",
            "order": 2
        },
        {
            "name": "source2target_file",
            "prefix": "",
            "value_type": "File",
            "file_name": "transcripts2genes.txt",
            "order": 3
        },
        {
            "name": "output_prefix",
            "prefix": "../app/results/{{id}}/",
            "value_type": "None",
            "order": 4
        }
    ],
    "result": [
        {
            "name" : "Microalignment",
            "type": "File",
            "path": ""
        }
    ]
}
```