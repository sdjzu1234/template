<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, title, wrapper_id	***/
	function ct_content($tag)
	{
		return '
		<div id="'.$tag['attributes']->id.'">'.$tag['attributes']->title.'</div>
		<div id="'.$tag['attributes']->wrapper_id.'">
			<ul></ul>
		</div>';
	}
?>