<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, img_src 			***/
	function ct_divSection($tag)
	{
		$divBar = '<div style="background-color:#003300; font-size:20px; font-style: arial" id="'.$tag['attributes']->id.'"><h3>'.$tag['attributes']->text.'</h3></div>';

		if (property_exists($tag['attributes'], 'radio'))
		{
			$result = '<input type="radio" id='.$tag['attributes']->id.' name='.$tag['attributes']->id.'> '.$divBar.'<br>';

			return $result;
		}

		$result = $divBar;

		return $result;
	}
?>