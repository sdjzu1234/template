<?php
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id, title, wrapper_id	***/
	function ct_sequenceSelector($tag)
	{
		return '
		<div id="addSequence">
			<ct:button 	id="addSequenceBtn" text="Add Sequence(s)" 	onclick="inputShowHide()""/>
		</div>
		<ct:content 	id="RNAListTitle" 	title="Sequence list" 	wrapper_id="rnaList-content-wrapper"/>
		<div id="rnaList-actionPanel">
			<ct:button 	id="selectButton" 	text="Select All" 		onclick="selectAll()"/>
			<ct:button 	id="unselectButton" text="Unselect All" 	onclick="clearSelection()"/>
		</div>
		<div>
			<ct:button 	id="deleteButton" 	text="Delete Selected" 	onclick="deleteSequence()"	caution="true"/>
			<ct:button 	id="clearButton" 	text="Delete All" 		onclick="clearList()"		caution="true"/>
		</div>';
	}
?>