var express = require('express'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    dbconfig = require('./config/database.json');
    port = process.env.PORT || 3001,
    app = express();

// Enable CORS
app.use(cors());

// Database connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://'+ dbconfig.url + ':' + dbconfig.port + '/' + dbconfig.dbname, 
    {
        "user": dbconfig.username,
        "pass": dbconfig.password,
        useNewUrlParser: true
    });

// Register database schemas
Job = require('./models/job.js');

// Initialize bodyParser
app.use(bodyParser.urlencoded({ extended: true, limit: '20mb'  }));
app.use(bodyParser.json({ extended: true, limit: '20mb' }));

// Import routes
var routes = require('./routers/tools');
routes(app);

var server = app.listen(port, function () {
    console.log("app running on port.", server.address().port);
});
