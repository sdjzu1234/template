# -*- coding: utf-8 -*-

"""
`` **module description**:
This module allows to simulate biological sequence evolution throught sequence and splicing structure evolution events.
.. moduleauthor:: Esaie Kuitche - CoBIUS Team - Univeristy of Sherbrooke.
Juillet 2019

"""

import copy
import numpy as np
import random
from random import shuffle
import itertools
from ete3 import Tree
import argparse
import os

f = open("/var/www/applications/SimSpliceEvol/Example/onlineRunning/SimSpliceEvol_5d4c8490763a9/demofile2.txt", "a")
f.write("Now the file has more content!")
f.close()

#open and read the file after the appending:
f = open("/var/www/applications/SimSpliceEvol/Example/onlineRunning/SimSpliceEvol_5d4c8490763a9/demofile2.txt", "r")
print(f.read())

