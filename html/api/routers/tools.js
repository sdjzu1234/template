'use strict';
module.exports = function(app) {
  var toolController = require('../controllers/tools');

  app.route('/outil/:session')
    .post(toolController.execute);


  app.route('/outil/:session')
    .get(toolController.read);
};
