<?php
	
    namespace CustomTags;
	
	/*** 		Attributes 		***/
	/*** id 					***/
	function ct_text($tag)
	{
		if (property_exists($tag['attributes'], 'placeholder'))
		{
			return $tag['attributes']->text." ".'<input type= "text" id="'.$tag['attributes']->id.'" name="'.$tag['attributes']->name.' "placeholder="'.$tag['attributes']->placeholder.'"><br>';
		}
		return $tag['attributes']->text." ".'<input type= "text" id="'.$tag['attributes']->id.'" name="'.$tag['attributes']->name.'"><br>';
	}
?>