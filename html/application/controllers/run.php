<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Run extends CI_Controller {

	public function __construct()
   	{
       	parent::__construct();
    }
	
	public function index()
	{
		$changeInput = strip_tags(htmlspecialchars(trim($this->input->post('changeInput'))));
		$data['input']['files_gfasta'] = strip_tags(htmlspecialchars(trim($this->input->post('files_gfasta'))));
		$data['input']['files_tfasta'] = strip_tags(htmlspecialchars(trim($this->input->post('files_tfasta'))));
		$data['input']['files_t2gtxt'] = strip_tags(htmlspecialchars(trim($this->input->post('files_t2gtxt'))));
		
		$data['erreurs'] = array();
		if(empty($data['input']['files_gfasta'])){
			array_push($data['erreurs'], "Please specify the contents of the file gene.fasta");
		}
		if(empty($data['input']['files_tfasta'])){
			array_push($data['erreurs'], "Please specify the contents of the file transcript.fasta");
		}
		if(empty($data['input']['files_t2gtxt'])){
			array_push($data['erreurs'], "Please specify the contents of the file transcript2gene.txt");
		}
		
		// Si des erreurs ont été trouvées, on revient à la page principale
		if($changeInput == "1") $data['check1Echoue'] = false;
		else $data['check1Echoue'] = ! empty($data['erreurs']);
			
		if ($changeInput == "1" || $data['check1Echoue'])
		{
			$this->load->view('index', $data);
		}
		else
		{
			$this->load->view('run', $data);
		}
	}
}
